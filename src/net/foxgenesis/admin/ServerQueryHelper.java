/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin;

import static net.foxgenesis.util.io.LOCO.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import de.stefan1200.jts3serverquery.TS3ServerQueryException;
import net.foxgenesis.util.LoggerUtils;

/**
 * Function library to help with the server query
 * 
 * @author Seth
 */
public final class ServerQueryHelper {
	private static JTS3ServerQuery query = TeamspeakAdmin.query;

	/**
	 * Get client info of a user. If a client id is provided and the method
	 * fails, an error message will be sent to the client id
	 * 
	 * @param pattern
	 *            - username pattern
	 * @param clid
	 *            - client id to send error message to. Set to null for none.
	 * @return client info for username
	 */
	public static HashMap<String, String> clientInfo(String pattern, String clid) {
		HashMap<String, String> first = doCommand("clientfind pattern=" + pattern);
		if (!first.containsKey("id"))
			return doCommand("clientinfo clid=" + first.get("clid"));
		else if (clid != null)
			sendPM(clid, "Error: " + first.get("msg"));
		return null;
	}

	public static List<Client> getClients() {
		try {
			return Client.createClients(query.getList(JTS3ServerQuery.LISTMODE_CLIENTLIST,"-times,-groups"));
		} catch (TS3ServerQueryException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static List<Client> getClients(Predicate<Client> filter) {
		try {
			return Client.createAndFilterClients(query.getList(JTS3ServerQuery.LISTMODE_CLIENTLIST,"-times,-groups"), filter);
		} catch (TS3ServerQueryException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Get client info from client id
	 * 
	 * @param clid
	 *            - client id
	 * @return client info for client id
	 */
	public static HashMap<String, String> clientInfo(String clid) {
		return doCommand("clientinfo clid=" + clid);
	}

	/**
	 * Check if a client has a server group
	 * 
	 * @param clid
	 *            - client id
	 * @param groupid
	 *            - group id
	 * @return if the client has the group
	 */
	public static boolean hasServerGroup(String clid, String groupid) {
		HashMap<String, String> info = clientInfo(clid);
		if (info == null)
			return false;
		String[] groups = info.get("client_servergroups").split(",");
		return Arrays.asList(groups).contains(groupid);
	}

	/**
	 * Checks if a client has any of the server groups provided
	 * 
	 * @param clid
	 *            - client id
	 * @param groups
	 *            - array of group ids
	 * @return if the client has any of the groups
	 */
	public static boolean hasServerGroup(String clid, String[] groups) {
		for (String a : groups)
			if (hasServerGroup(clid, a))
				return true;
		return false;
	}

	public static void pokeClient(int clid, String reason) {
		doCommand("clientpoke clid=" + clid + " msg=" + query.encodeTS3String(reason));
	}

	public static boolean kickClient(int clid, String reason, boolean fromChannel) {
		try {
			LoggerUtils.info(main.format("kick", clid));
			query.kickClient(clid, fromChannel, reason);
			return true;
		} catch (TS3ServerQueryException e) {
			return false;
		}
	}

	/**
	 * Get the client id for a user
	 * 
	 * @param username
	 *            - username of client
	 * @return client id of client
	 */
	public static int getClid(String username) {
		return getClid(doCommand("clientfind pattern=" + username));
	}

	/**
	 * Get the client id from a client's info.<br>
	 * This is the same as {@code Integer.parseInt(info.get("clid"));}
	 * 
	 * @param info
	 *            - clients info
	 * @return the client id or -1 if not found
	 */
	public static int getClid(HashMap<String, String> info) {
		if (info.containsKey("clid"))
			return Integer.parseInt(info.get("clid"));
		return -1;
	}
	
	
	public static boolean moveClient(int clid, int cid, String password) {
		try {
			query.moveClient(clid, cid, password);
			return true;
		} catch (TS3ServerQueryException e) {
			return false;
		}
	}

	/**
	 * Sends the command to the server query. If the command fails due to lack
	 * of permissions, an error will be logged in console with permission name.
	 * 
	 * @param command
	 *            - command to be sent.
	 * @return result of the command
	 */
	public static HashMap<String, String> doCommand(String command) {
		if(TeamspeakAdmin.SLOW)
			try {
				Thread.sleep(TeamspeakAdmin.SLOW_RATE);
			} catch (InterruptedException e1) {}
		try {
			if (TeamspeakAdmin.DEBUG)
				LoggerUtils.debug(command);
			HashMap<String, String> data = query.doCommand(command);
			if (!data.get("id").equals("0")) {
				if (data.get("id").equals("2568"))
					LoggerUtils.error(data.get("msg") + ": "
							+ query.getPermissionInfo(Integer.parseInt(data.get("failed_permid"))).get("permname"));
				else
					LoggerUtils.printStack(data.get("msg") + " @ " + command);
				return data;
			}
			return query.parseLine(data.get("response"));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Checks if an event happened from a server query
	 * 
	 * @param event
	 *            - {@link TS3Event} type
	 * @param info
	 *            - event info
	 * @return if the event was from a server query
	 */
	public static boolean isQuery(TS3Event event, HashMap<String, String> info) {
		switch (event) {
		case CLIENT_MOVE:
		case CLIENT_JOIN:
			return isQueryFromAdv(info);
		case CLIENT_QUIT:
			return isQueryFromBasic(info);
		case CHANNEL_DELETED:
		case CHANNEL_EDITED:
		case TEXT_PRIVATE:
			return isQueryFromClid(info.get("invokerid"));
		default:
			LoggerUtils.error(event + " || " + info);
		}
		return true;
	}

	/**
	 * Checks if a client is a sever query from their advanced info
	 * 
	 * @param advancedInfo
	 *            - advanced info of the client
	 * @return if the client is a server query
	 */
	public static boolean isQueryFromAdv(HashMap<String, String> advancedInfo) {
		return advancedInfo.containsKey("client_type") && advancedInfo.get("client_type").equals("1");
	}

	/**
	 * Checks if a client is a server query from their client id
	 * 
	 * @param clid
	 *            - client id of the client
	 * @return if the client is a sever query
	 */
	public static boolean isQueryFromClid(String clid) {
		return isQueryFromAdv(clientInfo(clid));
	}

	/**
	 * Checks if a client is a server query from their basic info(Something that
	 * contains their client id)
	 * 
	 * @param basicInfo
	 *            - basic info of the client
	 * @return if the client is a server query
	 */
	public static boolean isQueryFromBasic(HashMap<String, String> basicInfo) {
		return isQueryFromClid(basicInfo.get("clid"));
	}

	/**
	 * Send a private message to a client
	 * 
	 * @param clid
	 *            - client id of the client
	 * @param message
	 *            - message to be sent
	 */
	public static void sendPM(int clid, String message) {
		doCommand("sendtextmessage targetmode=1 target=" + clid + " msg=" + query.encodeTS3String(message));
	}

	/**
	 * Send a private message to a client
	 * 
	 * @param clid
	 *            - client id of the client
	 * @param message
	 *            - message to be sent
	 */
	public static void sendPM(String clid, String message) {
		sendPM(Integer.parseInt(clid), message);
	}

	/**
	 * Logs a given message to the teamspeak server log at a given level
	 * 
	 * @param level
	 *            - {@link LogLevel} of the log
	 * @param msg
	 *            - message to log
	 */
	public static void log(LogLevel level, String msg) {
		doCommand("logadd loglevel=" + level.getID() + " logmsg=" + query.encodeTS3String(msg));
	}

	/**
	 * Finds a channel based off its name
	 * 
	 * @param pattern
	 *            - pattern to use
	 * @return response from server query
	 */
	public static HashMap<String, String> findChannel(String pattern) {
		return doCommand("channelfind pattern=" + query.encodeTS3String(pattern));
	}

	/**
	 * Gets the channel info from a channel id
	 * 
	 * @param cid
	 *            - channel id
	 * @return channel info if found
	 */
	public static HashMap<String, String> channelInfo(int cid) {
		return doCommand("channelinfo cid=" + cid);
	}

	/**
	 * Filter all the channel
	 * 
	 * @param filter
	 *            {@link Predicate} to filter though the channels
	 * @return {@link List} of filtered channels
	 */
	public static List<HashMap<String, String>> filterChannels(Predicate<HashMap<String, String>> filter) {
		try {
			return query.getList(JTS3ServerQuery.LISTMODE_CHANNELLIST).stream().filter(filter)
					.collect(Collectors.toList());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Delete channel from a channel id
	 * 
	 * @param cid
	 *            - channel id
	 * @param force
	 *            - force delete even if there are clients
	 */
	public static void deleteChannel(int cid, boolean force) {
		try {
			query.deleteChannel(cid, force);
		} catch (TS3ServerQueryException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Edit a channel from a channel id with given arguments
	 * 
	 * @param cid
	 *            - channel id
	 * @param arguments
	 *            - {@link Map} of arguments
	 */
	public static void editChannel(int cid, Map<ChannelProperties, String> arguments) {
		String args = "";
		for (Entry<ChannelProperties, String> a : arguments.entrySet())
			args += " " + a.getKey().name().toLowerCase() + "=" + a.getValue();
		query.doCommand("channeledit cid=" + cid + args);
	}

	/**
	 * Gets the channel id from {@link #findChannel(String) findChannel}
	 * 
	 * @param pattern
	 *            - pattern to use
	 * @return channel id
	 */
	public static int getChannelId(String pattern) {
		return Integer.parseInt(findChannel(pattern).get("cid"));
	}

	/**
	 * Create a channel in the teamspeak 3 server
	 * 
	 * @param name
	 *            - name of the channel
	 * @param perm
	 *            - if the channel should be perm
	 * @param parentId
	 *            - parent channel id or -1 for none
	 */
	public static void createChannel(String name, boolean perm, int parentId) {
		doCommand("channelcreate channel_name=" + query.encodeTS3String(name) + " channel_flag_permanent="
				+ (perm ? 1 : 0) + (parentId != -1 ? " cpid=" + parentId : ""));
	}

	/**
	 * Gets the sub channels of a channel
	 * 
	 * @param cid
	 *            - channel id
	 * @return list of sub channel ids
	 */
	public static List<Integer> getSubChannels(int cid) {
		ArrayList<Integer> list = new ArrayList<>();
		try {
			query.getList(JTS3ServerQuery.LISTMODE_CHANNELLIST).stream()
					.filter(channel -> channel.get("pid").equals("" + cid))
					.forEach(d -> list.add(Integer.parseInt(d.get("cid"))));
		} catch (TS3ServerQueryException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Gets the sub channels of a channel
	 * 
	 * @param pattern
	 *            - channel name
	 * @return list of sub channel ids
	 */
	public static List<Integer> getSubChannels(String pattern) {
		return getSubChannels(getChannelId(pattern));
	}

	/**
	 * LogLevel is the level of which messages are to be logged on the teamspeak
	 * server
	 * 
	 * @author Seth
	 */
	public static enum LogLevel {
		/**
		 * Something bad happened
		 */
		ERROR(1),

		/**
		 * Something shouldn't have happened, but it's ok.
		 */
		WARNING(2),

		/**
		 * Helpful messages to fix bugs
		 */
		DEBUG(3),

		/**
		 * Normal logging type
		 */
		INFO(4);

		private final int id;

		LogLevel(int id) {
			this.id = id;
		}

		/**
		 * Get the ID number of the {@link LogLevel}
		 * 
		 * @return {@link LogLevel} ID number
		 */
		public int getID() {
			return id;
		}
	}
	
	public static enum Codec {
		CODEC_SPEEX_NARROWBAND(0),
		CODEC_SPEEX_WIDEBAND(1),
		CODEC_SPEEX_ULTRAWIDEBAND(2),
		CODEC_CELT_MONO(3);
		
		private final int id;
		Codec(int id) {
			this.id = id;
		}
		
		public static Codec getForNumber(int n) {
			for(Codec a:values())
				if(a.id == n)
					return a;
			return null;
		}
	}
}
