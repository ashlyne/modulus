/**
    Copyright (C) 2015  FoxGenesis
 
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.commands;

import java.util.ArrayList;
import java.util.List;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import de.stefan1200.jts3serverquery.TS3ServerQueryException;
import net.foxgenesis.admin.Client;
import net.foxgenesis.admin.ServerQueryHelper.LogLevel;
import net.foxgenesis.admin.plugin.AbstractChatCommand;
import net.foxgenesis.admin.plugin.AbstractFunction;
import net.foxgenesis.util.io.ActiveFileList;

/**
 * @author Seth
 *
 */
@ChatCommand(name = "blacklist", usage = "[add <regex> | remove <regex>]")
public class BlackListCommand extends AbstractChatCommand {
	private final ActiveFileList blacklist;
	private final List<Integer> users;

	public BlackListCommand(ActiveFileList blacklist, AbstractFunction function, ArrayList<Integer> users) {
		super(function);
		this.blacklist = blacklist;
		this.users = users;
	}

	@Override
	public boolean handleCommand(Client client, String[] args, JTS3ServerQuery query) throws TS3ServerQueryException {
		if (args != null && args.length == 2) {
			if (args[0].equalsIgnoreCase("add")) {
				blacklist.append(args[1]);
				log(LogLevel.INFO, client.getNickName() + " added [" + args[1] + "] to the blacklist.");
				client.sendMessage("Added " + args[1] + " to the blacklist");
			} else if (args[0].equalsIgnoreCase("remove")) {
				blacklist.remove(args[1]);
				log(LogLevel.INFO, client.getNickName() + " removed [" + args[1] + "] from the blacklist.");
				client.sendMessage("Removed " + args[1] + " from the blacklist");
			}
			return false;
		} else {
			log(LogLevel.INFO, client.getNickName() + " looked at the blacklist");
			client.sendMessage(blacklist.getList().toString());
		}
		return true;
	}

	@Override
	public boolean canClientInvoke(Client client) {
		return client.hasAServerGroup(users);
	}
}
