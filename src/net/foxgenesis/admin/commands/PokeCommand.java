/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.commands;

import java.util.List;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import net.foxgenesis.admin.Client;
import net.foxgenesis.admin.TeamspeakAdmin;
import net.foxgenesis.admin.plugin.AbstractChatCommand;
import net.foxgenesis.util.ArrayHelper;
import net.foxgenesis.util.LoggerUtils;
import net.foxgenesis.util.StringHelper;

/**
 * @author Seth
 *
 */
@ChatCommand(name = "poke", usage = "<username> <reason>")
public class PokeCommand extends AbstractChatCommand {

	@Override
	public boolean canClientInvoke(Client client) {
		return true;
	}

	@Override
	public boolean handleCommand(Client client, String[] args, JTS3ServerQuery query) throws Exception {
		if (args != null && args.length >= 2) {
			String msg = StringHelper.deSplit(ArrayHelper.rest(args), " ");
			List<Client> clients = TeamspeakAdmin.getCache().findClient(args[0]);
			LoggerUtils.debug("Found " + clients.size() + " clients in search.");
			if(clients.size() > 1)
				client.sendMessage("More than one client found. Please refine.");
			else if(clients.isEmpty())
				client.sendMessage("No client found");
			else
				clients.forEach(c -> c.poke(msg));
			return true;
		}
		return false;
	}

}
