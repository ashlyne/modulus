/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.commands;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import net.foxgenesis.admin.Client;
import net.foxgenesis.admin.TeamspeakAdmin;
import net.foxgenesis.admin.plugin.AbstractChatCommand;
import net.foxgenesis.util.UpdaterJar;
import net.foxgenesis.util.Version;
import net.foxgenesis.util.io.SiteReader;
import net.foxgenesis.util.io.StreamHelper;

/**
 * @author Seth
 */
@ChatCommand(name = "forceupdate")
public class ForceUpdateCommand extends AbstractChatCommand {

	@Override
	public boolean canClientInvoke(Client client) {
		return false;
	}

	@Override
	public boolean handleCommand(Client client, String[] args, JTS3ServerQuery query) throws Exception {
		Version v = new Version(SiteReader.getHTML(new URL(TeamspeakAdmin.isBeta()?TeamspeakAdmin.BETA_VERSION_URL:TeamspeakAdmin.VERSION_URL)));
		if(v.isGreaterThan(TeamspeakAdmin.VERSION)) {
			client.sendMessage("Update found. Installing.");
			File f = new File("Updater.jar");
			if(f.exists())
				f.delete();
			f.createNewFile();
			StreamHelper.copy(UpdaterJar.class.getResourceAsStream("/Updater.jar"),new FileOutputStream(f));
			String fileName = new File(UpdaterJar.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getName();
			Runtime.getRuntime().exec(new String[]{"java","-jar",f.toString(),fileName,TeamspeakAdmin.isBeta()?TeamspeakAdmin.BETA_JAR_URL:TeamspeakAdmin.JAR_URL,""+false});
			System.exit(0);
		} else
			client.sendMessage("No Update available");
		return true;
	}

}
