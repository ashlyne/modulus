/**
    Copyright (C) 2015  FoxGenesis
 
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.commands;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import net.foxgenesis.admin.plugin.AbstractChatCommand;

/**
 * Annotation to tell the class loader that this is a
 * {@link AbstractChatCommand}. Also used for chat command creation.
 * 
 * @author Seth
 * @see AbstractChatCommand
 * @see FunctionCommand
 */
@Retention(value = RetentionPolicy.RUNTIME)
public @interface ChatCommand {

	/**
	 * Command name. This is the name that is used when calling the function.
	 * <br>
	 * (i.e. <i>{@code !name args...}</i>)
	 * 
	 * @return
	 */
	public String name();

	/**
	 * This explains to the user how to use the command. This is displayed when
	 * they type <i>!help</i>.<br>
	 * (i.e. <i>{@code !kick <username, reason>}</i>)
	 * 
	 * @return
	 */
	public String usage() default "No Arguments";
}
