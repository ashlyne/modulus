/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin;

import static org.fusesource.jansi.Ansi.ansi;

import java.io.File;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.fusesource.jansi.AnsiConsole;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import net.foxgenesis.admin.controller.AdminController;
import net.foxgenesis.admin.controller.DataSection;
import net.foxgenesis.admin.plugin.PluginManager;
import net.foxgenesis.admin.plugin.TeamspeakAdminController;
import net.foxgenesis.util.LoggerUtils;
import net.foxgenesis.util.UpdaterJar;
import net.foxgenesis.util.Version;
import net.foxgenesis.util.io.ConfigurationFile;
import net.foxgenesis.util.io.LOCO;

/**
 * Where the magic happens
 * 
 * @author Seth
 */
public final class TeamspeakAdmin {
	
	protected static JTS3ServerQuery query;
	protected static TS3Cache cache;
	private static PluginManager manager;

	protected static boolean DEBUG = false;
	protected static boolean VERBOSE = false;
	protected static boolean BETA = false;
	private static boolean allowUpdates = true;

	public static final Version VERSION = new Version("0.2.1");

	public static final String VERSION_URL = "https://bitbucket.org/ssteinb/modulus/downloads/version";
	public static final String JAR_URL = "https://bitbucket.org/stteinb/modulus/downloads/TS3Admin.jar";

	public static final String BETA_VERSION_URL = "https://dl.dropboxusercontent.com/u/60916534/TS3Admin/version";
	public static final String BETA_JAR_URL = "https://dl.dropboxusercontent.com/u/60916534/TS3Admin/TS3Admin.jar";

	public static List<Integer> ADMIN_RANKS;

	protected static boolean SLOW = false;
	protected static long SLOW_RATE = 1000;
	
	private static boolean allowNatives = true;
	private static boolean allowBuiltIn = true;
	
	private static final Timer keepalive = new Timer("Keep Alive");
	
	private static AdminController webpanel;

	static {
		LoggerUtils.info("#######################");
		LoggerUtils.info("#       Modulus	      #");
		LoggerUtils.info("#######################");
		LoggerUtils.info("Author: Seth Steinberg");
		LoggerUtils.line();
		AnsiConsole.systemInstall();
	}
	
	public static boolean isSlowMode() {
		return SLOW;
	}
	
	public static long getSlowRate() {
		return SLOW_RATE;
	}
	
	public static boolean isDebug() {
		return DEBUG;
	}
	
	public static boolean isVerbose() {
		return VERBOSE;
	}
	
	public static boolean isBeta() {
		return BETA;
	}
	
	public static boolean nativesAllowed() {
		return allowNatives;
	}
	
	public static TS3Cache getCache()  {
		return cache;
	}
	
	public static boolean allowUpdates() {
		return allowUpdates;
	}
	
	public static boolean allowBuiltInFunctions() {
		return allowBuiltIn;
	}

	public static void main(String[] args) throws Exception {
		java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "true");
		java.lang.System.setProperty("https.protocols", "TLSv1,SSLv3,SSLv2Hello");
		
		LOCO lang = LOCO.main;
		
		boolean allowEvents = true;
		boolean login = true;
		File keystore = null;
		char[] kpassword = null;
		for(int i=0; i<args.length; i++) {
			String a = args[i];
			if (a.equalsIgnoreCase("-debug")) {
				DEBUG = true;
				LoggerUtils.debug(lang.get("debug-enable"));
			} else if (a.equalsIgnoreCase("-noEvents"))
				allowEvents = false;
			else if (a.equalsIgnoreCase("-noLogin"))
				login = false;
			else if (a.equalsIgnoreCase("-verbose")) {
				VERBOSE = true;
				LoggerUtils.verbose(lang.get("verbose-enable"));
			}
			else if(a.equalsIgnoreCase("-beta")) {
				LoggerUtils.info(lang.get("beta-enable"));
				BETA = true;
			} else if(a.equalsIgnoreCase("-keystore") && i + 2 < args.length) {
				String d = args[i+1];
				if(d.startsWith("-"))
					throw new UnsupportedOperationException(lang.get("keystore-init-error"));
				keystore = new File(d);
				kpassword = args[i+2].toCharArray();
				i+=2;
			}
		}
		// Load bot config
		LoggerUtils.info("Loading config...");
		ConfigurationFile c = new ConfigurationFile(
				TeamspeakAdmin.class.getResourceAsStream("/assets/net/foxgenesis/admin/config"), new File("config"));
		String ip = c.getString("ip");
		int port = c.getInt("port");
		int qport = c.getInt("query-port");
		String username = c.getString("username");
		String password = c.getString("password");
		String threadName = c.containsKey("thread-name") ? c.getString("thread-name") : "TS3 Admin";
		String name = c.getString("bot-name");
		int webport = c.getInt("web-port");
		String webip = c.getString("web-ip");
		allowNatives = c.getBoolean("allow-natives");
		ADMIN_RANKS = c.getIntegerList("admins");
		SLOW = c.getBoolean("slow");
		SLOW_RATE = c.getLong("slow-rate");
		allowUpdates = c.getBoolean("allow-updates");
		allowBuiltIn = c.getBoolean("allow-built-in-functions");
		
		if(BETA) {
			LoggerUtils.line();
			LoggerUtils.line();
			LoggerUtils.warning("###########################################################");
			LoggerUtils.warning(lang.get("beta-line1"));
			LoggerUtils.warning(lang.get("beta-line2"));
			LoggerUtils.warning("###########################################################");
			LoggerUtils.info(lang.get("beta-line3"));
			Thread.sleep(3000);
			if(allowUpdates)
				UpdaterJar.run(VERSION, BETA_VERSION_URL, BETA_JAR_URL, args, false);
		} else if(allowUpdates)
			UpdaterJar.run(VERSION, VERSION_URL, JAR_URL, args, false);
		TeamspeakAdminController.addHandler("overview", () -> {
			return new DataSection("Overview")
					.add("Debug", DEBUG)
					.add("Verbose",VERBOSE)
					.add("Beta", BETA)
					.add("Version", VERSION).toString();
		});
		
		if(keystore != null && kpassword != null) {
			LoggerUtils.info(lang.get("webpanel-load"));
			webpanel = new TeamspeakAdminController(new InetSocketAddress(webip,webport),keystore,kpassword);
			LoggerUtils.info(lang.get("webpanel-loaded"));
			LoggerUtils.line();
		} else LoggerUtils.warning(lang.get("error-webpanel-keystore"));

		// Connect to the teamspeak
		query = new JTS3ServerQuery(threadName);
		query.DEBUG = DEBUG;
		LoggerUtils.info(lang.get("connecting"));
		c.onUpdate(config -> {
			try {
				query.setDisplayName(config.getString("bot-name"));
				ADMIN_RANKS = config.getIntegerList("admins");
				SLOW = config.getBoolean("slow");
				SLOW_RATE = config.getLong("slow-rate");
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		do {
			try {
				LoggerUtils.info(lang.get("connect-attempt"));
				query.connectTS3Query(ip, qport);
			} catch(Exception e) {
				e.printStackTrace();
				LoggerUtils.info(lang.get("connect-retry"));
				Thread.sleep(30_000);
			}
		} while(!query.isConnected());
		query.selectVirtualServer(port, true);
		if (login) {
			LoggerUtils.info(lang.get("login"));
			query.loginTS3(username, password);
		} else
			LoggerUtils.info(lang.get("no-login"));
		query.setDisplayName(name);
		cache = new TS3Cache(query);
		LoggerUtils.verbose(String.format(lang.get("connect"),query.getCurrentQueryClientID()));
		LoggerUtils.line();

		// Create query listener
		LoggerUtils.info(lang.get("listener-init"));
		manager = new PluginManager(query, allowEvents);
		cache.forceUpdate(TS3Cache.MODE_ALL);

		// Load the plugins
		LoggerUtils.info(lang.get("plugin-load"));
		manager.loadPlugins();

		// Create functions
		LoggerUtils.info(lang.get("function-load"));
		manager.initFunctions();

		// Create keep alive
		LoggerUtils.line();
		LoggerUtils.info(lang.get("keep-alive-create"));
		keepalive.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				query.doCommand("whoami");
			}
		}, 60000, 60000);

		LoggerUtils.info(ansi().reset().render(lang.get("load-finish")));
		LoggerUtils.line();
		
		webpanel.start();
		while (query.isConnected())
			Thread.sleep(1000);
		LoggerUtils.warning(lang.get("connect-lost"));
		AnsiConsole.systemUninstall();
		Thread.sleep(1000);
		keepalive.cancel();
		System.gc();
		main(args);
	}
}