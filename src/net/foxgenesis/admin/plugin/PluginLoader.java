/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.plugin;

import static net.foxgenesis.util.io.LOCO.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Pattern;

import net.foxgenesis.admin.plugin.util.PluginDescriptionFile;

/**
 * This is where more black magic occurs. Again, just don't touch.
 * 
 * @author Seth
 * @author Bukkit
 */
public final class PluginLoader {

	private final Pattern[] fileFilters = new Pattern[] { Pattern.compile("\\.jar$"), };
	private final Map<String, Class<?>> classes = new HashMap<String, Class<?>>();
	private final Map<String, PluginClassLoader> loaders = new LinkedHashMap<String, PluginClassLoader>();

	public AbstractPlugin loadPlugin(final File file, EventManager events) {
		try {
			if (!file.exists())
				throw new FileNotFoundException(file.getPath() + " does not exist");

			final PluginDescriptionFile description;
			description = getPluginDescription(file);

			final File parentFile = file.getParentFile();
			final File dataFolder = new File(parentFile, description.getName());
			if (!dataFolder.exists())
				dataFolder.mkdir();
			final File oldDataFolder = new File(parentFile, description.getRawName());

			// Found old data folder
			if (dataFolder.equals(oldDataFolder))
				;
			else if (dataFolder.isDirectory() && oldDataFolder.isDirectory()) {
				System.err.println(
						String.format("While loading %s (%s) found old-data folder: `%s' next to the new one `%s'",
								description.getFullName(), file, oldDataFolder, dataFolder));
			} else if (oldDataFolder.isDirectory() && !dataFolder.exists()) {
				if (!oldDataFolder.renameTo(dataFolder)) {
					throw new Exception(
							"Unable to rename old data folder: `" + oldDataFolder + "' to: `" + dataFolder + "'");
				}
				System.err.println(String.format("While loading %s (%s) renamed data folder: `%s' to `%s'",
						description.getFullName(), file, oldDataFolder, dataFolder));
			}

			if (dataFolder.exists() && !dataFolder.isDirectory()) {
				throw new Exception(
						String.format("Projected datafolder: `%s' for %s (%s) exists and is not a directory",
								dataFolder, description.getFullName(), file));
			}

			final PluginClassLoader loader;
			loader = new PluginClassLoader(this, getClass().getClassLoader(), description, dataFolder, file, events);
			loaders.put(description.getName(), loader);

			return loader.plugin;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	protected PluginDescriptionFile getPluginDescription(File file) {
		JarFile jar = null;
		InputStream stream = null;
		try {
			jar = new JarFile(file);
			JarEntry entry = jar.getJarEntry("plugin.config");
			if (entry == null)
				throw new FileNotFoundException(String.format(main.get("no-plugin-config"),file.toString()));
			stream = jar.getInputStream(entry);
			return new PluginDescriptionFile(stream);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (jar != null)
				try {
					jar.close();
				} catch (IOException e) {
				}
			if (stream != null)
				try {
					stream.close();
				} catch (IOException e) {
				}
		}
		return null;
	}

	public Pattern[] getPluginFileFilters() {
		return fileFilters.clone();
	}

	Class<?> getClassByName(final String name) {
		Class<?> cachedClass = classes.get(name);

		if (cachedClass != null) {
			return cachedClass;
		} else {
			for (String current : loaders.keySet()) {
				PluginClassLoader loader = loaders.get(current);

				try {
					cachedClass = loader.findClass(name, false);
				} catch (ClassNotFoundException cnfe) {
				}
				if (cachedClass != null) {
					return cachedClass;
				}
			}
		}
		return null;
	}

	void setClass(final String name, final Class<?> clazz) {
		if (!classes.containsKey(name))
			classes.put(name, clazz);
	}
}
