/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.plugin.util;

import static net.foxgenesis.util.io.LOCO.main;

import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.swing.JOptionPane;

import net.foxgenesis.util.LoggerUtils;
import net.foxgenesis.util.Version;
import net.foxgenesis.util.io.SiteReader;
import net.foxgenesis.util.io.StreamHelper;

/**
 * @author Seth
 *
 */
public final class PluginUpdater {
	//TODO fix this cancer
	public static void update(File file) {
		PluginDescriptionFile f = getPluginDescription(file);
		if(f.getUpdateURL() == null || f.getVersionURL() == null)
			return;
		LoggerUtils.verbose(main.format("plugin-update-check", f.getName()));
		try {
			Version server = new Version(SiteReader.getHTML(new URL(f.getVersionURL())));
			if(f.getVersion().isLessThan(server))
				update(file,f);
		} catch (MalformedURLException e) {
			LoggerUtils.error(main.get("invalid-version-url"));
		} catch (IOException e) {
			LoggerUtils.error(main.format("version-read-error", e.getMessage()));
		}
	}
	
	private static void update(File file, PluginDescriptionFile desc) {
		boolean gui = !GraphicsEnvironment.isHeadless();
		if(gui) {
			int option = JOptionPane.showConfirmDialog(null, "Update available for: " + desc.getName(), "New Update Available", JOptionPane.YES_NO_OPTION);
			if(option != JOptionPane.YES_OPTION)
				return;
		} else {
			System.out.println("New Update available for " + desc.getName());
			String answer = "";
			do {
				System.out.println("Would you like to update? [y/n]: ");
				answer = System.console().readLine();
			} while (!(answer.equalsIgnoreCase("y") || answer.equalsIgnoreCase("n")));
			if(!answer.equalsIgnoreCase("y"))
				return;
		}
		file.delete();
		if(file.exists()) {
			LoggerUtils.error("Failed to delete old plugin");
			return;
		}
		try {
			URL url = new URL(desc.getUpdateURL());
			file.createNewFile();
			FileOutputStream out = new FileOutputStream(file);
			LoggerUtils.verbose("Updating plugin...");
			StreamHelper.copy(url.openStream(), out);
			LoggerUtils.verbose("Plugin updated!");
		} catch (IOException e) {
			LoggerUtils.error("Error occured while trying to update plugin");
		}
	}

	private static PluginDescriptionFile getPluginDescription(File file) {
		JarFile jar = null;
		InputStream stream = null;
		try {
			jar = new JarFile(file);
			JarEntry entry = jar.getJarEntry("plugin.config");
			if (entry == null)
				throw new FileNotFoundException("Jar does not contain plugin.config");
			stream = jar.getInputStream(entry);
			return new PluginDescriptionFile(stream);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (jar != null)
				try {
					jar.close();
				} catch (IOException e) {
				}
			if (stream != null)
				try {
					stream.close();
				} catch (IOException e) {
				}
		}
		return null;
	}
}
