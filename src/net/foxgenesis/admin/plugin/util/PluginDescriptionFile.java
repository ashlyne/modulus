/**
    Copyright (C) 2015  FoxGenesis
 
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.plugin.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import net.foxgenesis.util.LoggerUtils;
import net.foxgenesis.util.Version;

/**
 * This is a plugin's configuration
 * 
 * @author Seth
 */
public final class PluginDescriptionFile {
	private String name, raw, main, help, versionURL, updateURL;
	private Version version;
	private boolean natives;

	public PluginDescriptionFile(InputStream stream) {
		try (BufferedReader r = new BufferedReader(new InputStreamReader(stream))) {
			r.lines().filter(line -> !line.trim().startsWith("#") && !line.isEmpty()).forEach(line -> {
				line = line.trim();
				String[] d = line.split(":", 2);
				if (d.length == 2) {
					switch (d[0].toLowerCase().trim()) {
					case "name":
						raw = d[1].trim();
						break;
					case "version":
						version = new Version(d[1].trim());
						break;
					case "main":
						main = d[1].trim();
						break;
					case "help":
						help = d[1].trim();
						break;
					case "versionURL":
						versionURL = d[1].trim();
						break;
					case "updateURL":
						updateURL = d[1].trim();
						break;
					case "natives":
						natives = Boolean.parseBoolean(d[1].trim());
						break;
					}
				} else
					LoggerUtils.warning("Invalid line in configuration: " + line);
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		name = raw.replaceAll(" ", "_");
	}

	/**
	 * Get the name with the version added on
	 * @return name and version
	 */
	public String getFullName() {
		return getName() + " v" + getVersion();
	}

	/**
	 * Get the name of the plugin.
	 * All spaces are replaced with '_'
	 * @return name of the plugin
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get the version of the plugin
	 * @return plugin version
	 */
	public Version getVersion() {
		return version;
	}

	/**
	 * Get location of the main plugin class
	 * @return location of plugin class
	 */
	public String getMain() {
		return main;
	}
	
	/**
	 * Gets the help message to display
	 * @return help message
	 */
	public String getHelpMessage() {
		return help;
	}

	/**
	 * Get the raw name of the plugin. 
	 * The raw name is exactly how it displays in the plugin.conf
	 * @return raw name of the plugin
	 */
	public String getRawName() {
		return raw;
	}
	
	/**
	 * Returns the url to find the current version of the plugin
	 * @return url containing current version of the plugin
	 */
	public String getVersionURL() {
		return versionURL;
	}
	
	/**
	 * Returns the url to find the latest build of the plugin
	 * @return url containing latest build
	 */
	public String getUpdateURL() {
		return updateURL;
	}
	
	/**
	 * Returns true if the plugin uses natives
	 * @return Does the plugin use natives
	 */
	public boolean hasNatives() {
		return natives;
	}
}
