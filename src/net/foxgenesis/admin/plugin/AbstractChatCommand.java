/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.plugin;

import static net.foxgenesis.util.io.LOCO.main;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import net.foxgenesis.admin.Client;
import net.foxgenesis.admin.ServerQueryHelper;
import net.foxgenesis.admin.ServerQueryHelper.LogLevel;
import net.foxgenesis.admin.commands.ChatCommand;
import net.foxgenesis.util.LoggerUtils;
import net.foxgenesis.util.io.LOCO;

/**
 * Foundation for chat commands. This class requires the {@link ChatCommand}
 * annotation for the class loader to find it and for creation of the command.
 * 
 * @author Seth
 * @see ChatCommand
 * @see FunctionCommand
 */
public abstract class AbstractChatCommand {
	private final String name;
	private final String usage;
	private final AbstractFunction function;
	private String help;
	private LOCO loco;

	public AbstractChatCommand() {
		this(null);
	}

	public AbstractChatCommand(AbstractFunction function) {
		if (getClass().isAnnotationPresent(ChatCommand.class)) {
			ChatCommand f = getClass().getAnnotation(ChatCommand.class);
			this.name = f.name();
			this.usage = f.usage();
		} else {
			LoggerUtils.error(String.format(main.get("missing-annotation"),getClass()));
			this.name = "null";
			this.usage = "null";
		}
		this.function = function;
		if(function != null)
			this.loco = function.getLOCO();
	}

	/**
	 * Gets the calling name of the command
	 * 
	 * @return command's name
	 */
	public final String getName() {
		return name;
	}

	protected final void info(Object msg) {
		LoggerUtils.info("[" + (function != null?function.getName():name) + "]: " + msg);
	}

	protected final void log(LogLevel level, Object msg) {
		ServerQueryHelper.log(level, "[" + (function != null?function.getName():name) + "]: " + msg);
	}


	protected LOCO getLOCO() {
		return loco;
	}

	/**
	 * Gets the usage of the command
	 * 
	 * @return command's usage
	 */
	public final String getUsage() {
		return usage;
	}

	public final String getDisplayUsage() {
		return "[b]" + name + ": [/b]" + usage;
	}

	@Override
	public final boolean equals(Object j) {
		if (j instanceof AbstractChatCommand)
			return ((AbstractChatCommand) j).getName().equalsIgnoreCase(name);
		return false;
	}

	//TODO add javadoc
	protected final void setHelpMessage(String msg) {
		this.help = msg;
	}

	//TODO add javadoc
	public final String getHelpMessage() {
		return help;
	}

	//TODO add javadoc
	public abstract boolean canClientInvoke(Client client);

	/**
	 * Handle the command
	 * 
	 * @param clInfo
	 *            - client info
	 * @param clid
	 *            - client id
	 * @param args
	 *            - arguments of the command
	 * @param query
	 *            - {@link JTS3ServerQuery} to use
	 * @return if the command's usage shouldn't be displayed
	 * @throws Exception
	 *             in case of anything
	 */
	public abstract boolean handleCommand(Client client, String[] args, JTS3ServerQuery query) throws Exception;
}
