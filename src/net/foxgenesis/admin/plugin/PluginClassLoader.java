package net.foxgenesis.admin.plugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import net.foxgenesis.admin.plugin.util.PluginDescriptionFile;
import net.foxgenesis.util.JNIHelper;
import net.foxgenesis.util.LoggerUtils;
import net.foxgenesis.util.io.StreamHelper;

/**
 * This is where the black magic occurs. Just don't touch. Thank god Bukkit
 * released its code.
 * 
 * @author Seth
 * @author Bukkit
 */
public final class PluginClassLoader extends URLClassLoader {
	private final PluginLoader loader;
	private final Map<String, Class<?>> classes = new HashMap<String, Class<?>>();
	private final PluginDescriptionFile description;
	private final File dataFolder;
	private final File file;
	final AbstractPlugin plugin;
	private AbstractPlugin pluginInit;
	private EventManager events;

	PluginClassLoader(final PluginLoader loader, final ClassLoader parent, final PluginDescriptionFile description,
			final File dataFolder, final File file, EventManager events)
					throws UnsupportedOperationException, MalformedURLException {
		super(new URL[] { file.toURI().toURL() }, parent);

		this.loader = loader;
		this.description = description;
		this.dataFolder = dataFolder;
		this.file = file;
		this.events = events;

		try {
			Class<?> jarClass;
			try {
				jarClass = Class.forName(description.getMain(), true, this);
			} catch (ClassNotFoundException ex) {
				throw new UnsupportedOperationException("Cannot find main class `" + description.getMain() + "'", ex);
			}

			Class<? extends AbstractPlugin> pluginClass;
			try {
				pluginClass = jarClass.asSubclass(AbstractPlugin.class);
			} catch (ClassCastException ex) {
				throw new UnsupportedOperationException(
						"main class `" + description.getMain() + "' does not extend JavaPlugin", ex);
			}

			plugin = pluginClass.newInstance();

			// force load all classes so its on list with the class loader
			JarFile jarFile = new JarFile(file);
			Enumeration<JarEntry> e = jarFile.entries();
			File natives = null;
			boolean nativesFound = false;
			if(plugin.getDesc().hasNatives())
				natives = new File(plugin.getDataFolder(), "natives");
			while (e.hasMoreElements()) {
				JarEntry je = (JarEntry) e.nextElement();
				if(je.getName().equalsIgnoreCase("natives.zip")) {
					nativesFound = true;
					if(!natives.exists()) {
						natives.mkdir();
						LoggerUtils.verbose("Natives found for " + description.getName());
						extractNatives(jarFile.getInputStream(je),natives);
					}
				}
				if (je.isDirectory() || !je.getName().endsWith(".class"))
					continue;
				String className = je.getName().substring(0, je.getName().length() - 6);
				className = className.replace('/', '.');
				try {
					Class<?> c = loadClass(className);
					@SuppressWarnings("unused")
					Class<?> s = c.getSuperclass();
					//load any important classes here
				} catch (Exception e1) {}
			}
			System.gc();
			jarFile.close();
			if(natives != null && !nativesFound)
				LoggerUtils.warning(description.getName() + " wanted natives but none were found!");
			else {
				LoggerUtils.verbose("Loding plugin natives");
				JNIHelper.addLibraryFolder(natives);
			}
		} catch (IllegalAccessException ex) {
			throw new UnsupportedOperationException("No public constructor", ex);
		} catch (InstantiationException ex) {
			throw new UnsupportedOperationException("Abnormal plugin type", ex);
		} catch (IOException e1) {
			throw new UnsupportedOperationException("Failed to create all classes", e1);
		}
	}

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		return findClass(name, true);
	}

	Class<?> findClass(String name, boolean checkGlobal) throws ClassNotFoundException {
		Class<?> result = classes.get(name);

		if (result == null) {
			if (checkGlobal)
				result = loader.getClassByName(name);
			if (result == null) {
				result = super.findClass(name);
				if (result != null)
					loader.setClass(name, result);
			}
			classes.put(name, result);
		}
		return result;
	}

	Set<String> getClasses() {
		return classes.keySet();
	}

	synchronized void initialize(AbstractPlugin javaPlugin) {
		if (this.plugin != null || this.pluginInit != null)
			return;
		this.pluginInit = javaPlugin;
		javaPlugin.load(dataFolder, file, description, this, events);
	}

	private static void extractNatives(InputStream stream, File nativesFolder) {
		File f = new File(nativesFolder, "natives.zip");
		try {
			createFile(stream,f);
			unzip(f,nativesFolder);
		}catch(Exception e) {
			LoggerUtils.debug(nativesFolder);
			e.printStackTrace();
		}
	}

	private static void createFile(InputStream stream, File f) throws IOException {
		f.createNewFile();
		try(FileOutputStream out = new FileOutputStream(f)) {
			StreamHelper.copy(stream, out);
		}
	}

	private static void unzip(File zipFile, File folder){
		byte[] buffer = new byte[1024];
		try {
			ZipInputStream zis = 
					new ZipInputStream(new FileInputStream(zipFile));
			ZipEntry ze = zis.getNextEntry();
			while(ze != null) {	
				String fileName = ze.getName();
				File newFile = new File(folder + File.separator + fileName);       
				new File(newFile.getParent()).mkdirs();
				FileOutputStream fos = new FileOutputStream(newFile);             
				int len;
				while ((len = zis.read(buffer)) > 0)
					fos.write(buffer, 0, len);
				fos.close();   
				ze = zis.getNextEntry();
			}
			zis.closeEntry();
			zis.close();
		} catch(IOException ex){
			ex.printStackTrace(); 
		}
	}    
}