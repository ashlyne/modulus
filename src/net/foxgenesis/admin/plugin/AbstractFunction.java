/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.plugin;

import static net.foxgenesis.util.io.LOCO.main;

import java.io.File;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import net.foxgenesis.admin.DefaultFeatures;
import net.foxgenesis.admin.ServerQueryHelper;
import net.foxgenesis.admin.ServerQueryHelper.LogLevel;
import net.foxgenesis.admin.controller.ControllerData;
import net.foxgenesis.admin.functions.Function;
import net.foxgenesis.util.LoggerUtils;
import net.foxgenesis.util.io.AHTML;
import net.foxgenesis.util.io.ConfigurationFile;
import net.foxgenesis.util.io.LOCO;

/**
 * Foundation for functions. {@link Function} annotation is <b>required</b>.
 * 
 * @author Seth
 * @see Function
 * @see FunctionCommand
 */
public abstract class AbstractFunction implements ControllerData {
	private static final AHTML ahtml = new AHTML(AbstractFunction.class.getResourceAsStream("/assets/net/foxgenesis/admin/webpanel/function.ahtml"));
	
	private String name;
	private ConfigurationFile config;
	private AbstractPlugin plugin;
	private String help;
	private LOCO loco;
	private boolean enabled = true;

	public AbstractFunction(AbstractPlugin plugin) {
		this.plugin = plugin;
		this.loco = plugin.getLOCO();
		if (getClass().isAnnotationPresent(Function.class)) {
			Function f = this.getClass().getAnnotation(Function.class);
			this.name = f.name();
			config = f.useConfig() ? plugin instanceof DefaultFeatures
					? new ConfigurationFile(
							getClass().getResourceAsStream("/assets/net/foxgenesis/admin/functions/"
									+ name.toLowerCase().replaceAll(" ", "_")),
							new File("plugins/" + name.replaceAll(" ", "_") + "/cfg/" + name.toLowerCase().replaceAll(" ", "_")
									+ ".config"))
							: new ConfigurationFile(plugin.getClassLoader(), plugin.getDataFolder(),
									name.toLowerCase().replaceAll(" ", "_") + ".config")
							: null;
							if(config != null)
								enabled = config.containsKey("enabled")?config.getBoolean("enabled"):true;
		} else {
			LoggerUtils.error(String.format(main.get("missing-annotation"),getClass()));
			this.name = "null";
			config = null;
		}
	}

	/**
	 * Gets the name of the function
	 * 
	 * @return function name
	 */
	public final String getName() {
		return name;
	}
	
	protected LOCO getLOCO() {
		return loco;
	}

	/**
	 * Gets the data folder for the plugin
	 * 
	 * @return data folder of plugin
	 */
	public File getDataFolder() {
		return plugin.getDataFolder();
	}

	public EventManager getEventManager() {
		return plugin.getEventManager();
	}

	/**
	 * Gets the configuration file.<br>
	 * <b>This will throw an error if the function doesn't use a configuration
	 * file
	 * 
	 * @return function's configuration file
	 */
	protected ConfigurationFile getConfig() {
		if (config != null)
			return config;
		else
			throw new UnsupportedOperationException("Function [" + name + "] doesn't use a configuration file");
	}

	/**
	 * Set the help message to be displayed for the help command
	 * @param msg - help message to be displayed
	 */
	protected void setHelpMessage(String msg) {
		this.help = msg;
	}

	/**
	 * Get the help message displayed for the help command
	 * @return help message for the function
	 */
	public String getHelpMessage() {
		return help;
	}

	/**
	 * Logs a message to console
	 * 
	 * @param msg
	 *            - message to log
	 */
	protected void info(Object msg) {
		LoggerUtils.info(name, msg);
	}

	protected void error(Object msg, boolean fatal) {
		if(fatal) {
			enabled = false;
			LoggerUtils.error("**** FATAL **** [" + name + "]: " + msg);
		} else LoggerUtils.error(name, msg);
	}

	/**
	 * Logs a message to console if debug is enabled
	 * 
	 * @param msg
	 *            - message to log
	 */
	protected void debug(Object msg) {
		LoggerUtils.debug(name, msg);
	}

	/**
	 * Logs a message to console if verbose is enabled
	 * 
	 * @param msg
	 *            - message to log
	 */
	protected void verbose(Object msg) {
		LoggerUtils.verbose(name, msg);
	}

	/**
	 * Checks configuration file for the enabled key
	 * 
	 * @return should the function be enabled
	 */
	protected boolean isEnabled() {
		return enabled;
	}

	/**
	 * Log a custom log to the server
	 * 
	 * @param level
	 *            - {@link LogLevel} to use
	 * @param msg
	 *            - Message to log
	 */
	protected void log(LogLevel level, Object msg) {
		ServerQueryHelper.log(level, "[" + name + "]: " + msg);
	}
	
	protected void preInit(JTS3ServerQuery query) {
		info("Loading...");
		init(query);
		LoggerUtils.render("[" + name + "]: @|green Enabled!|@");
	}

	/**
	 * Creation of the function
	 * 
	 * @param query
	 *            - {@link JTS3ServerQuery} for use
	 */
	protected abstract void init(JTS3ServerQuery query);

	/**
	 * Register a chat command for this function
	 * @param command - command to assign to this function
	 */
	protected void registerChatCommand(AbstractChatCommand command) {
		plugin.registerChatCommand(command);
	}
	
	@Override
	public String getHTMLData() {
		return String.format(ahtml.get("function"), name);
	}

}
