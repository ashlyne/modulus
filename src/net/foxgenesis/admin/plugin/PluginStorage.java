/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.plugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import net.foxgenesis.admin.Client;
import net.foxgenesis.admin.DefaultFeatures;
import net.foxgenesis.admin.TeamspeakAdmin;
import net.foxgenesis.admin.commands.ChatCommand;
import net.foxgenesis.util.ArrayHelper;
import net.foxgenesis.util.LoggerUtils;

/**
 * @author Seth
 *
 */
public class PluginStorage {
	private final HashMap<String,AbstractChatCommand> commands = new HashMap<>();
	private final HashMap<String,AbstractFunction> functions = new HashMap<>();
	protected final HashMap<String,AbstractPlugin> plugins = new HashMap<>();

	private static List<String> pK,cK,fK;
	private final JTS3ServerQuery query;

	private static final int MODE_COMMANDS = 0, MODE_FUNCTIONS = 1, MODE_PLUGINS = 2;

	private final List<AbstractPlugin> pluginList;

	private final PluginManager manager;

	public PluginStorage(List<AbstractPlugin> pluginList, JTS3ServerQuery query, PluginManager manager) {
		this.query = query;
		this.pluginList = pluginList;
		this.manager = manager;
	}

	public void finish() {
		LoggerUtils.verbose("Sorting plugin names...");
		pK = new ArrayList<>();
		pluginList.forEach(plugin -> {
			if(plugin instanceof DefaultFeatures)
				pK.add("Default_Features");
			else
				pK.add(plugin.getDesc().getName().replaceAll(" ", "_").toLowerCase());	
		});
		Collections.sort(pK);

		LoggerUtils.verbose("Sorting function names...");
		Collections.sort(fK = Arrays.asList(this.functions.keySet().toArray(new String[]{})));

		LoggerUtils.verbose("Sorting command names...");
		Collections.sort(cK = Arrays.asList(this.commands.keySet().toArray(new String[]{})));
	}

	public void addFunction(AbstractFunction f) {
		if(functions.containsKey(f.getName()))
			LoggerUtils.warning("Duplicate function [" + f.getName() + "]. Not adding.");
		else {
			LoggerUtils.verbose("Adding function: " + f.getName().replaceAll(" ", "_"));
			functions.put(f.getName().replaceAll(" ", "_").toLowerCase(), f);
			manager.registerFunction(f);
		}
	}

	public void addCommand(AbstractChatCommand c) {
		if(commands.containsKey(c.getName()))
			LoggerUtils.warning("Duplicate command [" + c.getName() + "]. Not adding.");
		else {
			LoggerUtils.verbose("Adding command: " + c.getName());
			commands.put(c.getName().replaceAll(" ", "_").toLowerCase(), c);
		}
	}

	public void handleCommand(Client client, String command, String[] args) {
		try {
			if (commands.containsKey(command)) {
				AbstractChatCommand c = commands.get(command);
				if (c.canClientInvoke(client) || client.hasAServerGroup(TeamspeakAdmin.ADMIN_RANKS))
					try {
						if (!c.handleCommand(client, args, query))
							client.sendMessage("[b]Usage: [/b]" + c.getDisplayUsage());
					} catch (Exception e) {
						e.printStackTrace();
					}
				else
					client.sendMessage("[b][color=red]Sorry Steve, but I can't let you do that.[/color][/b]");
			} else
				client.sendMessage("[b][color=red]No Command Found: [/color]" + command + "[/b]");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	protected AbstractChatCommand[] defaultCommands() {
		return new AbstractChatCommand[]{
				new HelpCommand(commands,functions,plugins),
				new PluginsCommand(),
				new FunctionsCommand(),
				new CommandsCommand()
		};
	}


	@ChatCommand(name = "help", usage = "[command | function | plugin]")
	public static class HelpCommand extends AbstractChatCommand {
		private final HashMap<String,AbstractChatCommand> commands;
		private final HashMap<String,AbstractFunction> functions;
		private final HashMap<String,AbstractPlugin> plugins;

		public HelpCommand(HashMap<String,AbstractChatCommand> commands, 
				HashMap<String,AbstractFunction> functions, 
				HashMap<String,AbstractPlugin> plugins) {
			this.commands = commands;
			this.functions = functions;
			this.plugins = plugins;
			setHelpMessage("By default, help will search commands."
					+ "\nTo specify, please use the following arguments:\n"
					+ "-c <command name>\tTo specify a command\n"
					+ "-f <function name>\tTo specify a function\n"
					+ "-p <plugin name>\tTo specify a plugin");
		}

		@Override
		public boolean canClientInvoke(Client client) {
			return true;
		}

		@Override
		public boolean handleCommand(Client client, String[] args, JTS3ServerQuery query) throws Exception {
			if (args == null || args.length == 0) {
				client.sendMessage("Displaying commands...");
				cK.forEach(client::sendMessage);
				return true;
			} else if(args.length == 1) {
				
				return true;
			} else {
				int m = MODE_COMMANDS;
				switch(args[0].toLowerCase()) {
				case "-f":
					m = MODE_FUNCTIONS;
					break;
				case "-p":
					m = MODE_PLUGINS;
					break;
				}
				help(client,Arrays.asList(ArrayHelper.rest(args)).stream().reduce("",(a,b) -> a + "_" + b),m);
				return true;
			}
		}

		private void help(Client client, String name, int mode) {
			name = name.toLowerCase().substring(1);
			HashMap<String,?> map;
			if(mode == MODE_COMMANDS)
				map = commands;
			else if(mode == MODE_FUNCTIONS)
				map = functions;
			else 
				map = plugins;
			if(map.containsKey(name)) {
				switch(mode) {
				case MODE_COMMANDS:
					if(commands.get(name).getHelpMessage() != null) {
						client.sendMessage(commands.get(name).getHelpMessage());
						return;
					}
					break;
				case MODE_FUNCTIONS:
					if(functions.get(name).getHelpMessage() != null) {
						client.sendMessage(functions.get(name).getHelpMessage());
						return;
					}
					break;
				case MODE_PLUGINS:
					if(plugins.get(name).getDesc().getHelpMessage() != null) {
						client.sendMessage(plugins.get(name).getDesc().getHelpMessage());
						return;
					}
					break;
				}
				client.sendMessage("No help provided. Sorry.");
			} else
				client.sendMessage("[b][color=red]Not found: " + name + "[/color][/b]");
		}
	}

	@ChatCommand(name = "plugins", usage = "no arguments")
	public class PluginsCommand extends AbstractChatCommand {

		@Override
		public boolean canClientInvoke(Client client) {
			return false;
		}

		@Override
		public boolean handleCommand(Client client, String[] args, JTS3ServerQuery query) throws Exception {
			client.sendMessage("Total plugins: " + pK.size());
			pK.forEach(client::sendMessage);
			return true;
		}

	}

	@ChatCommand(name = "functions", usage = "no arguments")
	public class FunctionsCommand extends AbstractChatCommand {

		@Override
		public boolean canClientInvoke(Client client) {
			return false;
		}

		@Override
		public boolean handleCommand(Client client, String[] args, JTS3ServerQuery query) throws Exception {
			client.sendMessage("Total functions: " + fK.size());
			fK.forEach(client::sendMessage);
			return true;
		}
	}

	@ChatCommand(name = "commands", usage = "no arguments")
	public class CommandsCommand extends AbstractChatCommand {

		@Override
		public boolean canClientInvoke(Client client) {
			return true;
		}

		@Override
		public boolean handleCommand(Client client, String[] args, JTS3ServerQuery query) throws Exception {
			client.sendMessage("Total commands: " + cK.size());
			cK.forEach(client::sendMessage);
			return true;
		}
	}
}
