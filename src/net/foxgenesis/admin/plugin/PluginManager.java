/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.plugin;

import static net.foxgenesis.util.io.LOCO.main;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import de.stefan1200.jts3serverquery.TS3ServerQueryException;
import net.foxgenesis.admin.DefaultFeatures;
import net.foxgenesis.admin.TeamspeakAdmin;
import net.foxgenesis.admin.plugin.util.PluginUpdater;
import net.foxgenesis.util.LoggerUtils;
import net.foxgenesis.util.io.AHTML;

/**
 * @author Seth
 *
 */
public class PluginManager {
	private static final String ROOT = "/assets/net/foxgenesis/admin/webpanel/";
	private static final AHTML aP = new AHTML(PluginManager.class.getResourceAsStream(ROOT + "plugin.ahtml"));
	private final PluginLoader loader = new PluginLoader();
	private final EventManager events;
	private ArrayList<AbstractPlugin> plugins = new ArrayList<>();
	private ArrayList<AbstractFunction> functions = new ArrayList<>();

	private final JTS3ServerQuery query;

	private String sidebar;

	public PluginManager(JTS3ServerQuery query, boolean events) throws TS3ServerQueryException {
		this.query = query;
		this.events = new EventManager(plugins, this, query, events);
		if (events) {
			LoggerUtils.info(main.get("create-events"));
			query.addEventNotify(JTS3ServerQuery.EVENT_MODE_TEXTPRIVATE, 0);
			if(TeamspeakAdmin.isSlowMode())
				try {
					Thread.sleep(TeamspeakAdmin.getSlowRate());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			query.addEventNotify(JTS3ServerQuery.EVENT_MODE_TEXTSERVER, 0);
			if(TeamspeakAdmin.isSlowMode())
				try {
					Thread.sleep(TeamspeakAdmin.getSlowRate());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			query.addEventNotify(JTS3ServerQuery.EVENT_MODE_SERVER, 0);
		} else
			LoggerUtils.info(main.get("no-events"));
		TeamspeakAdminController.addHandler("plugins-page", () -> {return "<div class=\"content section\">" + sidebar +  "</div>";});
	}

	public void registerFunction(AbstractFunction function) {
		functions.add(function);
	}

	public void initFunctions() {
		functions.stream().filter(AbstractFunction::isEnabled).forEach(f -> f.preInit(query));
		setSidebar();
	}

	public void registerPlugin(AbstractPlugin plugin) {
		plugins.add(plugin);
	}

	public boolean checkForDuplicate(String path) {
		return plugins.stream().anyMatch(p -> p.getDesc().getMain().equals(path));
	}

	private void setSidebar() {
		String output = "";
		for(AbstractPlugin a: plugins) {
			boolean b = (a instanceof DefaultFeatures);
			String n = b?"Default_Features":a.getDesc().getName();
			output += String.format(aP.get("plugin"), 
					n,b?"Default Features":a.getDesc().getRawName());
			TeamspeakAdminController.instance.addPlugin("/plugins/" + n,formatPluginPage(a));
		}
		sidebar = String.format(aP.get("plugin-list"), output);
	}
	
	private String formatPluginPage(AbstractPlugin a) {
		boolean b = (a instanceof DefaultFeatures);
		String output = aP.get("plugin-page");
		output = output.replaceAll("%NAME%", b?"Default Features":a.getDesc().getRawName());
		return output;
	}

	public void loadPlugins() {
		DefaultFeatures fe = new DefaultFeatures(events.storage.defaultCommands());
		plugins.add(fe);
		fe.load(events, query);
		try {
			File f = new File("plugins");
			if (!f.exists()) {
				LoggerUtils.info(main.get("no-plugin-folder"));
				if(!f.mkdir())
					LoggerUtils.error(main.get("plugin-folder-fail"));
			}
			if (f.listFiles() != null) {
				if(TeamspeakAdmin.allowUpdates())
					Arrays.asList(f.listFiles(file -> file.getName().endsWith(".jar"))).forEach(PluginUpdater::update);
				Arrays.asList(f.listFiles(file -> file.getName().endsWith(".jar"))).forEach(file -> {
					LoggerUtils.line();
					LoggerUtils.info(main.format("plugin-file-load",file.getName()));
					AbstractPlugin plugin = loader.loadPlugin(file, events);
					plugins.add(plugin);
					String pluginName = plugin.getDesc().getFullName();
					LoggerUtils.verbose(main.format("plugin-init",pluginName));
					plugin.init(query);
					LoggerUtils.render(main.format("plugin-loaded",pluginName));
					LoggerUtils.line();
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		events.finishUp();
	}
}
