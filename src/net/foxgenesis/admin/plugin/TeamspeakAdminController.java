/**
    Copyright (C) 2015  FoxGenesis
 
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.plugin;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import net.foxgenesis.admin.controller.AdminController;

/**
 * @author Seth
 *
 */
public class TeamspeakAdminController extends AdminController {
	private static final String ROOT = "assets/net/foxgenesis/admin/webpanel/";
	protected static TeamspeakAdminController instance;

	public TeamspeakAdminController(InetSocketAddress address, File keystore, char[] password) throws IOException, UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException {
		super(address, keystore, password);
		instance = this;
		this.preloadPage(ROOT + "index.html");
		this.preloadPage(ROOT + "plugins.html");
		this.preloadPage(ROOT + "login.html");
		this.addAsset("text/css", "/style.css", ROOT + "style.css",false);
		this.addAsset("text/javascript", "/js/mobile.js", ROOT + "mobile.js");
		this.addAsset("text/javascript", "/js/post.js", ROOT + "post.js",false);
		this.addAsset("text/css", "/mobile.css", ROOT + "mobile.css");
		this.addAsset("image/ico", "/favicon.ico", ROOT + "favicon.ico");
		setIndexPage(t -> printPage(ROOT + "index.html",t));
		this.addPage("/plugins",t -> this.printPageIfLoggedIn(ROOT + "plugins.html", t));
		this.addPostPage("/save-config", (file,data) -> getSaves().get(file).save(data));
	}
	
	public void addPlugin(String path, String page) {
		this.addPage(path, t -> {
			if(isUserLoggedIn(t))
				finish(t, page, 200);
			else printPage("assets/net/foxgenesis/admin/webpanel/login.html",t);
		});
	}
}
