/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import net.foxgenesis.admin.Client;
import net.foxgenesis.admin.ServerQueryHelper;
import net.foxgenesis.admin.TS3Cache;
import net.foxgenesis.admin.TS3Event;
import net.foxgenesis.admin.TeamspeakAdmin;
import net.foxgenesis.admin.events.ClientJoinEvent;
import net.foxgenesis.admin.events.ClientJoinEvent.ClientJoinHandler;
import net.foxgenesis.admin.events.ClientQuitEvent;
import net.foxgenesis.admin.events.ClientQuitEvent.ClientQuitHandler;
import net.foxgenesis.util.ArrayHelper;
import net.foxgenesis.util.LoggerUtils;

/**
 * @author Seth
 */
public class EventManager {

	private final boolean events;
	protected final PluginStorage storage;

	public EventManager(List<AbstractPlugin> plugins, PluginManager manager, JTS3ServerQuery query, boolean events) {
		this.events = events;
		storage = new PluginStorage(plugins, query, manager);
		query.setTeamspeakActionListener((type, info) -> {
			TS3Event event = TS3Event.getFor(type);
			if (event == TS3Event.TEXT_PRIVATE)
				new Thread(()->handleChat(info),"Chat Thread").start();
			else 
				new Thread(()->handleEvent(event, info),"Event Thread").start();
		});
	}
	
	public void finishUp() {
		storage.finish();
	}

	private void handleChat(HashMap<String, String> info) {
		try {
			String id = info.get("invokerid");
			HashMap<String, String> clInfo = ServerQueryHelper.clientInfo(id);
			if (info.get("msg").startsWith("!")) {
				String msg = info.get("msg");
				String[] command = msg.split(" ");
				storage.handleCommand(new Client(Integer.parseInt(id), clInfo), command[0].substring(1), ArrayHelper.rest(command));
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	protected void handleEvent(TS3Event event, HashMap<String, String> info) {
		if (!events) 
			return;
		LoggerUtils.verbose(event + " | " + info);
		Client client = null;
		if (event.isClientEvent()) {
			if(event != TS3Event.CLIENT_QUIT)
				TeamspeakAdmin.getCache().forceUpdate(TS3Cache.MODE_CLIENT);
			int clid = Integer.parseInt(info.get("clid"));
			LoggerUtils.debug("[EVENT] CLIENT ID: " + clid);
			client = TeamspeakAdmin.getCache().getClientByID(clid);
			if(client == null)
				LoggerUtils.error("FAILED TO GET CLIENT FOR EVENT! : " + info);
		}
		switch (event) {
		case TEXT_PRIVATE:
			break;
		case CHANNEL_DELETED:
			break;
		case CHANNEL_EDITED:
			break;
		case CLIENT_JOIN:
			onJoin(new ClientJoinEvent(client));
			break;
		case CLIENT_MOVE:
			break;
		case CLIENT_QUIT:
			onQuit(new ClientQuitEvent(client));
			break;
		default:
			break;

		}
	}

	// ON JOIN
	private ArrayList<ClientJoinHandler> joins = new ArrayList<>();
	public void addJoinHandler(ClientJoinHandler j) {joins.add(j);}
	public void removeJoinHandler(ClientJoinHandler j) {joins.remove(j);}

	private void onJoin(ClientJoinEvent event) {
		new Thread(() -> joins.forEach(j -> j.clientJoin(event)),"On Join Thread").start();
	}

	// ON QUIT
	private ArrayList<ClientQuitHandler> leaves = new ArrayList<>();
	public void addQuitHandler(ClientQuitHandler j) {leaves.add(j);}
	public void removeQuitHandler(ClientQuitHandler j) {leaves.remove(j);}

	private void onQuit(ClientQuitEvent event) {
		new Thread(() -> leaves.forEach(j -> j.clientQuit(event)),"On Join Thread").start();
	}
}
