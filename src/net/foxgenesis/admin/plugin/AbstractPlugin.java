/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.plugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import net.foxgenesis.admin.plugin.util.PluginDescriptionFile;
import net.foxgenesis.util.LoggerUtils;
import net.foxgenesis.util.io.LOCO;

/**
 * Foundation for plugins. Required class for plugins.
 * 
 * @author Seth
 */
public abstract class AbstractPlugin {

	private File dataFolder;
	private PluginDescriptionFile desc;
	private File file;
	private ClassLoader loader;
	private EventManager events;
	private LOCO loco;

	/**
	 * Creates the new plugin. Do not call this. This is called by the class
	 * loader.
	 */
	protected AbstractPlugin() {
		final ClassLoader classLoader = this.getClass().getClassLoader();
		if (classLoader != ClassLoader.getSystemClassLoader()) {
			if (!(classLoader instanceof PluginClassLoader))
				throw new IllegalStateException("Plugin requires " + PluginClassLoader.class.getName());
			((PluginClassLoader) classLoader).initialize(this);
		}
	}

	/**
	 * Called by the main thread when its time for the plugin to load.
	 * 
	 * @param dataFolder
	 *            - data folder of the plugin
	 * @param file
	 *            - jar file of the plugin
	 * @param desc
	 *            - plugin's configuration
	 * @param loader
	 *            - plugin's class loader
	 */
	void load(File dataFolder, File file, PluginDescriptionFile desc, ClassLoader loader, EventManager events) {
		this.dataFolder = dataFolder;
		this.desc = desc;
		this.file = file;
		this.loader = loader;
		this.events = events;
		try {
			loco = LOCO.getLOCO(getClass());
		} catch(Exception e) {
			LoggerUtils.error(desc.getRawName(),"Unable to find localization for");
		}
	}

	/**
	 * Initialization of the plugin
	 * 
	 * @param query
	 *            - {@link JTS3ServerQuery} to use
	 */
	public abstract void init(JTS3ServerQuery query);

	/**
	 * Get the data folder of the plugin
	 * 
	 * @return plugin's data folder
	 */
	public File getDataFolder() {
		return dataFolder;
	}

	/**
	 * Get the plugin's configuration
	 * 
	 * @return plugin's configuration
	 */
	public PluginDescriptionFile getDesc() {
		return desc;
	}

	/**
	 * Get the {@link File} of the jar
	 * 
	 * @return plugin file
	 */
	public File getJar() {
		return file;
	}

	protected EventManager getEventManager() {
		return events;
	}

	protected void registerFunction(AbstractFunction function) {
		getEventManager().storage.addFunction(function);
	}

	protected void registerChatCommand(AbstractChatCommand command) {
		getEventManager().storage.addCommand(command);
	}

	protected void registerFunctions(AbstractFunction... functions) {
		Arrays.asList(functions).forEach(this::registerFunction);
	}

	protected void registerChatCommands(AbstractChatCommand... commands) {
		Arrays.asList(commands).forEach(this::registerChatCommand);
	}
	
	protected LOCO getLOCO() {
		return loco;
	}

	/**
	 * Get resource inside the jar file
	 * 
	 * @param filename
	 *            - filename to load
	 * @return wanted resource
	 */
	public InputStream getResource(String filename) {
		if (filename == null)
			throw new IllegalArgumentException("Filename cannot be null");
		try {
			URL url = getClassLoader().getResource(filename);
			if (url == null)
				return null;
			URLConnection connection = url.openConnection();
			connection.setUseCaches(false);
			return connection.getInputStream();
		} catch (IOException ex) {
			return null;
		}
	}

	/**
	 * Get the class loader that was used to make the plugin
	 * 
	 * @return plugin's class loader
	 */
	protected final ClassLoader getClassLoader() {
		return loader;
	}
}
