/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import net.foxgenesis.admin.commands.CacheCommand;
import net.foxgenesis.admin.commands.ExitCommand;
import net.foxgenesis.admin.commands.ForceUpdateCommand;
import net.foxgenesis.admin.commands.KickCommand;
import net.foxgenesis.admin.commands.PokeCommand;
import net.foxgenesis.admin.commands.VersionCommand;
import net.foxgenesis.admin.functions.BadNicknameFunction;
import net.foxgenesis.admin.functions.BotScoutFunction;
import net.foxgenesis.admin.functions.IdleMoverFunction;
import net.foxgenesis.admin.functions.UpdateNotifyFunction;
import net.foxgenesis.admin.plugin.AbstractChatCommand;
import net.foxgenesis.admin.plugin.AbstractPlugin;
import net.foxgenesis.admin.plugin.EventManager;
import net.foxgenesis.util.io.LOCO;

/**
 * @author Seth
 */
public class DefaultFeatures extends AbstractPlugin {
	private EventManager events;
	private final AbstractChatCommand[] defaultCommands;
	
	public DefaultFeatures(AbstractChatCommand[] defaultCommands) {
		this.defaultCommands = defaultCommands;
	}

	@Override
	public void init(JTS3ServerQuery query) {}

	public void load(EventManager events, JTS3ServerQuery query) {
		this.events = events;
		if(TeamspeakAdmin.allowBuiltInFunctions()) {
			registerChatCommand(new KickCommand());
			registerChatCommand(new PokeCommand());
			registerChatCommand(new VersionCommand());
			registerChatCommand(new ExitCommand());
			registerChatCommand(new CacheCommand());
			registerFunction(new IdleMoverFunction(this));
			registerFunction(new BotScoutFunction(this));
			registerFunction(new BadNicknameFunction(this));
			if(TeamspeakAdmin.allowUpdates()) {
				registerFunction(new UpdateNotifyFunction(this));
				registerChatCommand(new ForceUpdateCommand());
			}
		}
		registerChatCommands(defaultCommands);
	}
	
	@Override
	public LOCO getLOCO() {
		return LOCO.main;
	}

	@Override
	public EventManager getEventManager() {
		return events;
	}
}
