/**
    Copyright (C) 2015  FoxGenesis
 
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.foxgenesis.admin.ServerQueryHelper.Codec;
import net.foxgenesis.util.ArrayHelper;
import net.foxgenesis.util.LoggerUtils;

/**
 * @author Seth
 */
public class Channel {
	private final int cid;
	private final HashMap<String, String> data;

	public Channel(int cid) {
		this(cid,ServerQueryHelper.channelInfo(cid));
	}
	
	private Channel(int cid, HashMap<String,String> data) {
		this.cid = cid;
		this.data = data;
	}
	
	private Channel(HashMap<String,String> data) {
		this(Integer.parseInt(data.get("cid")),data);
	}

	//TODO javadoc
	public int getChannelID() {
		return cid;
	}

	//TODO javadoc
	public String getName() {
		return get("channel_name");
	}

	//TODO javadoc
	public String getTopic() {
		return get("channel_topic");
	}

	//TODO javadoc
	public String getDescription() {
		return get("channel_description");
	}

	//TODO javadoc
	public boolean isPassworded() {
		return check("channel_flag_password","1");
	}

	//TODO javadoc
	public int getMaxClients() {
		return getInt("channel_maxclients");
	}

	//TODO javadoc
	public int getMaxFamilyClients() {
		return getInt("channel_maxfamilyclients");
	}
	
	//TODO javadoc
	public Codec getCodec() {
		return Codec.getForNumber(getInt("channel_codec"));
	}
	
	//TODO javadoc
	public int getCodecQuality() {
		return getInt("channel_codec_quality");
	}
	
	//TODO finish method
	public Object getOrder() {
		throw new UnsupportedOperationException();
	}

	//TODO javadoc
	public boolean isPermanent() {
		return check("channel_flag_permanent","1");
	}

	//TODO javadoc
	public boolean isSemiPermanent() {
		return check("channel_flag_semi_permanent","1");
	}

	//TODO javadoc
	public boolean isTemporary() {
		return check("channel_flag_temporary","1");
	}

	//TODO javadoc
	public boolean isDefaultChannel() {
		return check("channel_flag_default","1");
	}

	//TODO javadoc
	public boolean isUnlimitedClients() {
		return check("channel_flag_maxclients_unlimited","1");
	}

	//TODO javadoc
	public boolean isUnlimitedFamilyClients() {
		return check("channel_flag_maxfamilyclients_unlimited","1");
	}

	//TODO javadoc
	public boolean isInheritedFamilyMaxClients() {
		return check("channel_flag_maxfamilyclients_inherited","1");
	}

	//TODO javadoc
	public int getNeededTalkPower() {
		return getInt("channel_needed_talk_power");
	}

	//TODO javadoc
	public String getPhoneticName() {
		return get("channel_name_phonetic");
	}

	//TODO javadoc
	public String getFilePath() {
		return get("channel_filepath");
	}

	//TODO javadoc
	public boolean isForcedSilence() {
		return check("channel_forced_silence","1");
	}
	
	//TODO finish method
	//TODO javadoc
	public Object getChannelIcon() {
		throw new UnsupportedOperationException();
	}

	//TODO javadoc
	public boolean isCodecUnencrypted() {
		return check("channel_codec_is_unencrypted","1");
	}

	//TODO javadoc
	public Channel getParentChannel() {
		return TeamspeakAdmin.cache.getChannelByID(getInt("cpid"));
	}

	//TODO javadoc
	public void edit(Map<ChannelProperties,String> properties) {
		ServerQueryHelper.editChannel(cid, properties);
	}

	//TODO javadoc
	public List<Client> getClients() {
		return TeamspeakAdmin.cache.filterClients(c -> c.getChannel().equals(this));
	}

	//TODO javadoc
	public List<Channel> getSubChannels() {
		return TeamspeakAdmin.cache.filterChannels(c -> c.getParentChannel().equals(this));
	}
	
	private String get(String key) {
		if(data.containsKey(key))
			return data.get(key);
		else
			LoggerUtils.error("Channel doesn't contain key: " + key);
		return null;
	}
	
	private int getInt(String key) {
		return Integer.parseInt(get(key));
	}
	
	private boolean check(String key, String value) {
		return key.equals(value);
	}

	@Override
	public String toString() {
		return data.toString();
	}
	
	@Override
	public boolean equals(Object j) {
		if(j instanceof Channel) {
			Channel h = (Channel)j;
			return h.cid == this.cid;
		}
		return false;
	}

	//TODO javadoc
	public static List<Channel> createChannels(Collection<HashMap<String,String>> channels){
		return Arrays.asList(createChannel(channels.iterator(), new Channel[]{}));
	}

	private static Channel[] createChannel(Iterator<HashMap<String, String>> channels, Channel[] list) {
		if (!channels.hasNext())
			return list;
		list = ArrayHelper.append(list, new Channel(channels.next()));
		return createChannel(channels, list);
	}
}
