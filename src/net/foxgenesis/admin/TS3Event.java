/**
    Copyright (C) 2015  FoxGenesis
 
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin;

/**
 * Possible events called from the teamspeak server query
 * 
 * @author Seth
 */
public enum TS3Event {
	/**
	 * Client joined the teamspeak server. <br>
	 * <i>"notifycliententerview"</i>
	 */
	CLIENT_JOIN("notifycliententerview", true, false),

	/**
	 * Client left the teamspeak server <br>
	 * <i>"notifyclientleftview"</i>
	 */
	CLIENT_QUIT("notifyclientleftview", true, false),

	/**
	 * Client sent private message to the bot <br>
	 * <i>"notifytextmessage"</i>
	 */
	TEXT_PRIVATE("notifytextmessage", false, false),

	/**
	 * Client was moved <br>
	 * <i>"notifyclientmoved"</i>
	 */
	CLIENT_MOVE("notifyclientmoved", true, false),

	CHANNEL_DELETED("notifychanneldeleted", false, true),

	CHANNEL_EDITED("notifychanneledited", false, true);

	private final boolean clientEvent, channelEvent;
	private final String name;

	TS3Event(String name, boolean clientEvent, boolean channelEvent) {
		this.name = name;
		this.clientEvent = clientEvent;
		this.channelEvent = channelEvent;
	}

	public boolean isClientEvent() {
		return clientEvent;
	}
	
	public boolean isChannelEvent() {
		return channelEvent;
	}

	/**
	 * Gets the {@link TS3Event} from the server query response name
	 * 
	 * @param name
	 *            - server query's response name
	 * @return {@link TS3Event} type
	 */
	public static TS3Event getFor(String name) {
		for (TS3Event a : values())
			if (a.name.equalsIgnoreCase(name))
				return a;
		System.err.println(name + " is not an event");
		return null;
	}
}
