/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import de.stefan1200.jts3serverquery.TS3ServerQueryException;
import net.foxgenesis.util.ListHelper;
import net.foxgenesis.util.LoggerUtils;

/**
 * @author Seth
 *
 */
public class TS3Cache {
	private HashMap<Integer, Client> clients = new HashMap<>();
	private HashMap<Integer, Channel> channels = new HashMap<>();
	private boolean clientRefresh = true, channelRefresh = true;
	private boolean updating = false;
	private long lastUpdate = 0;
	private static final long UPDATE = 20_000;
	public static final int MODE_CLIENT = 0, MODE_CHANNEL = 1, MODE_ALL = 3;
	private final JTS3ServerQuery query;
	private static ArrayList<Runnable> onUpdate = new ArrayList<>();

	public TS3Cache(JTS3ServerQuery query) {
		this.query = query;
		new Timer("Cache Update Timer").scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				update();
				onUpdate.forEach(Runnable::run);
			}
		}, UPDATE, UPDATE);
	}

	private void update() {
		if((updating || System.currentTimeMillis() - lastUpdate < UPDATE) && lastUpdate != -1)
			return;
		LoggerUtils.debug("TS3 Cache","Updating cache...");
		forceUpdate(MODE_ALL);
	}

	public void forceUpdate(int mode) {
		updating = true;
		if(mode == MODE_CLIENT || mode == MODE_ALL) {
			long t = System.currentTimeMillis();
			clientRefresh = true;
			synchronized(clients) {
				try {
					clients.clear();
					Client.createClients(query.getList(JTS3ServerQuery.LISTMODE_CLIENTLIST,"-uid,-away,-voice,-times,-groups,-info,-icon,-country,-ip")).forEach(c -> clients.put(c.getDatabaseID(), c));
				} catch (TS3ServerQueryException e) {
					e.printStackTrace();
				}
				clientRefresh = false;
				clients.notifyAll();
			}
			LoggerUtils.debug("TS3 Cache","Client cache: " + (System.currentTimeMillis() - t) + "ms");
			lastUpdate = System.currentTimeMillis();
		}
		if(mode == MODE_CHANNEL || mode == MODE_ALL) {
			long t = System.currentTimeMillis();
			//UPDATE CHANNELS
			synchronized(channels) {
				try {
					List<Channel> c = Channel.createChannels(query.getList(JTS3ServerQuery.LISTMODE_CHANNELLIST,"-topic,-flags,-voice,-limits,-icon,-secondsempty"));
					List<Channel> m = ListHelper.getMissing(c, channels.values());
					new Thread(() -> m.forEach(i -> {
						try {
							if(TeamspeakAdmin.SLOW)
								Thread.sleep(TeamspeakAdmin.SLOW_RATE);
							query.addEventNotify(JTS3ServerQuery.EVENT_MODE_CHANNEL, i.getChannelID());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}),"Channel listener update").start();
					channels.clear();
					c.forEach(i -> channels.put(i.getChannelID(), i));
				} catch (TS3ServerQueryException e) {
					e.printStackTrace();
				}
				channelRefresh = false;
				channels.notifyAll();
			}
			LoggerUtils.debug("TS3 Cache","Channel cache: " + (System.currentTimeMillis() - t) + "ms");
			lastUpdate = System.currentTimeMillis();
		}
		updating = false;
	}

	public Client getClientByDBID(int databaseID) {
		synchronized(clients) {
			while(clientRefresh)
				try {
					getState();
					clients.wait();
				} catch (InterruptedException e) {}
		}
		return clients.get(databaseID);
	}

	public List<Client> getClients() {
		synchronized(clients) {
			while(clientRefresh)
				try {
					getState();
					clients.wait();
				} catch(InterruptedException e) {}
		}
		return new ArrayList<>(clients.values());
	}

	public List<Client> findClient(String pattern) {
		synchronized(clients) {
			while(clientRefresh)
				try {
					getState();
					clients.wait();
				} catch (InterruptedException e) {}
		}
		return clients.values().stream().filter(c -> c.getNickName().contains(pattern)).collect(Collectors.toList());
	}

	public Channel getChannelByID(int cid) {
		return channels.get(cid);
	}

	public List<Channel> filterChannels(Predicate<Channel> filter) {
		return getChannels().stream().filter(filter).collect(Collectors.toList());
	}

	public List<Channel> getChannels() {
		synchronized(channels) {
			while(channelRefresh)
				try {
					getState();
					channels.wait();
				} catch(InterruptedException e) {}
		}
		return new ArrayList<>(channels.values());
	}

	public List<Channel> findChannel(String pattern) {
		synchronized(channels) {
			while(channelRefresh)
				try {
					getState();
					channels.wait();
				} catch (InterruptedException e) {}
		}
		return channels.values().stream().filter(c -> c.getName().contains(pattern)).collect(Collectors.toList());
	}

	public Client getClientByID(int clid) {
		try {
			return filterClients(c -> c.getClientID() == clid).get(0);
		} catch(Exception e) {
			return null;
		}
	}

	public List<Client> filterClients(Predicate<Client> filter) {
		return getClients().stream().filter(filter).collect(Collectors.toList());
	}

	public boolean isUpdatingClientCache() {
		return clientRefresh;
	}

	public boolean isUpdatingChannelCache() {
		return channelRefresh;
	}

	private void getState() {
		LoggerUtils.debug("TS3 Cache",Thread.currentThread().getName() + " waiting for cache update");
	}

	public static void addUpdateHandler(Runnable r) {
		onUpdate.add(r);
	}

	public static void removeUpdateHandler(Runnable r) {
		onUpdate.remove(r);
	}
}
