package net.foxgenesis.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import net.foxgenesis.util.ArrayHelper;

/* TODO connection_filetransfer_bandwidth_received=0
 * TODO connection_bytes_received_total=9206219
 * TODO client_meta_data=
 * TODO connection_packets_received_total=85594
 * TODO connection_connected_time=11998326
 * TODO client_month_bytes_uploaded=28153
 * TODO connection_bytes_sent_total=31763419
 * TODO client_default_token
 * TODO client_needed_serverquery_view_power=75
 * TODO client_base64HashClientUID=hfoidmolnhpaolhclincdhdklllmomglepmnnkfl
 * TODO client_version_sign=F0hY25Dtja0wcU6dzC39rNuYbhnDAbIwPHC3VO9Oicf13kUY2I2g6scPZ3p195Cw9gUYdBIRYm8ucHEhtSeWCw==
 * TODO client_flag_avatar=33e54494a2da28cd04f5b9193881844b
 * TODO client_total_bytes_downloaded=1567361
 * TODO client_outputonly_muted=0
 * TODO client_version=3.0.18.2 [Build: 1445512488]
 * TODO connection_packets_sent_total=245196
 * TODO connection_bandwidth_sent_last_minute_total=157
 * TODO client_created=1447108454
 * TODO client_default_channel=
 * TODO connection_filetransfer_bandwidth_sent=0
 * TODO client_total_bytes_uploaded=28153
 * TODO client_lastconnected=1452049681
 * TODO client_channel_group_inherited_channel_id=2224
 * TODO connection_bandwidth_received_last_minute_total=111
 * TODO connection_bandwidth_sent_last_second_total=81
 * TODO connection_bandwidth_received_last_second_total=83
 * TODO client_month_bytes_downloaded=334494
 */
public class Client {
	private final HashMap<String, String> data;
	private final int clid;
	private List<Integer> serverGroups = new ArrayList<>();

	public Client(HashMap<String, String> data) {
		this(Integer.parseInt(data.get("clid")), data);
	}

	public Client(int clid) {
		this(clid, ServerQueryHelper.clientInfo(clid + ""));
	}

	public Client(int clid, HashMap<String, String> data) {
		this.data = data;
		this.clid = clid;
		String[] g = getList("client_servergroups");
		Arrays.asList(g).forEach(d -> serverGroups.add(Integer.parseInt(d)));
	}

	/**
	 * Send a message to the client
	 * @param message - message to send
	 */
	public void sendMessage(String message) {
		ServerQueryHelper.sendPM(clid, message);
	}

	/**
	 * Poke the client with a given message
	 * @param message - message to use
	 */
	public void poke(String message) {
		if(!this.isQuery())
			ServerQueryHelper.pokeClient(clid, message);
	}
	
	public boolean kickFromChannel(String reason) {
		return ServerQueryHelper.kickClient(clid, reason, true);
	}
	
	public boolean kickFromServer(String reason) {
		return ServerQueryHelper.kickClient(clid, reason, false);
	}
	
	public boolean moveto(Channel channel) {
		return moveTo(channel,null);
	}
	
	public boolean moveTo(Channel channel, String password) {
		return ServerQueryHelper.moveClient(clid, channel.getChannelID(), password);
	}

	/**
	 * Get a list of server groups the client has
	 * @return
	 */
	public List<Integer> getServerGroups() {
		return serverGroups;
	}

	/**
	 * Check if the client has a server group
	 * @param id - server group id to check
	 * @return if the client has the group
	 */
	public boolean hasServerGroup(int id) {
		return getServerGroups().contains(id);
	}

	/**
	 * Check if the client has one of a list of server groups
	 * @param ids - server group ids to check
	 * @return if the client has one of the server groups
	 */
	public boolean hasAServerGroup(Collection<Integer> ids) {
		Iterator<Integer> d = ids.iterator();
		while (d.hasNext())
			if (getServerGroups().contains(d.next()))
				return true;
		return false;
	}

	/**
	 * Check if the client is currently recording
	 * @return if the client is recording
	 */
	public boolean isRecording() {
		return check("client_is_recording", "1");
	}

	/**
	 * Get client idle time in milliseconds
	 * @return client idle time (ms)
	 */
	public long getIdleTime() {
		return getLong("client_idle_time");
	}

	public int getTotalConnections() {
		return getInt("client_totalconnections");
	}

	/**
	 * Check if the client has overwolf installed
	 * @return if client has overwolf
	 */
	public boolean hasOverwolf() {
		return get("client_badges").endsWith("1");
	}

	//TODO add javadoc
	public String getSecurityHash() {
		return get("client_security_hash");
	}

	/**
	 * Get the clients nickname
	 * @return clients nickname
	 */
	public String getNickName() {
		return get("client_nickname");
	}

	/**
	 * Check if the client is a server query
	 * @return if client is a server query
	 */
	public boolean isQuery() {
		return get("client_type").equals("1");
	}

	/**
	 * Get client's IP address
	 * @return clients IP address
	 */
	public String getIP() {
		return get("connection_client_ip");
	}

	/**
	 * Get the clients unique ID
	 * @return clients unique ID
	 */
	public String getUID() {
		return get("client_unique_identifier");
	}

	/**
	 * Get the client's version of teamspeak
	 * @return client's version of teamspeak
	 */
	public String getClientVersion() {
		return get("client_version");
	}

	/**
	 * Get the client's operating system
	 * @return client's operating system
	 */
	public String getPlatform() {
		return get("client_platform");
	}

	//TODO add javadoc
	public boolean isMicMuted() {
		return check("client_input_muted", "1");
	}

	//TODO add javadoc
	public boolean isOutputMuted() {
		return check("client_output_muted", "1");
	}

	//TODO add javadoc
	public boolean isMicEnabled() {
		return check("client_input_hardware", "1");
	}

	//TODO add javadoc
	public boolean isOutputEnabled() {
		return check("client_output_hardware", "1");
	}

	//TODO add javadoc
	public String getDefaultChannel() {
		//TODO finish method
		throw new UnsupportedOperationException();
	}

	//TODO add javadoc
	public String getQueryLoginName() {
		return get("client_login_name");
	}

	/**
	 * Get the client's database ID
	 * @return client's database ID
	 */
	public int getDatabaseID() {
		return getInt("client_database_id");
	}

	/**
	 * Get the client's channel group
	 * @return client's channel group
	 */
	public int getChannelGroup() {
		return getInt("client_channel_group_id");
	}

	/**
	 * Check if the client has a channel group
	 * @param id - channel group ID to check
	 * @return if the client has the channel group
	 */
	public boolean hasChannelGroup(int id) {
		return getChannelGroup() == id;
	}

	/**
	 * Check if the client has a channel group from a list of groups
	 * @param ids - channel group ID list to use
	 * @return if the client has any of the channel groups listed
	 */
	public boolean hasAChannelGroup(Collection<Integer> ids) {
		return ids.contains(getChannelGroup());
	}

	/**
	 * Check if client is away
	 * @return if client is away
	 */
	public boolean isAway() {
		return check("client_away", "1");
	}

	/**
	 * Get the client's current channel
	 * @return client's current channel
	 */
	public Channel getChannel() {
		return TeamspeakAdmin.cache.getChannelByID(getInt("cid"));
	}

	/**
	 * Get the client's away message
	 * @return client's away message
	 */
	public String getAwayMessage() {
		return get("client_away_message");
	}

	//TODO add javadoc
	public boolean isAvatarSet() {
		//TODO finish method
		throw new UnsupportedOperationException();
	}

	/**
	 * Get the client's talk power
	 * @return client's talk power
	 */
	public int getTalkPower() {
		return getInt("client_talk_power");
	}

	/**
	 * Check if the client is requesting talk power
	 * @return if the client is requesting talk power
	 */
	public boolean isRequestingTalkPower() {
		return check("client_talk_request", "1");
	}

	/**
	 * Get the client's talk power request message
	 * @return client's talk power request message
	 */
	public String getTalkPowerRequestMessage() {
		return get("client_talk_request_msg");
	}

	/**
	 * Check if the client is currently talking
	 * @return if the client is currently talking
	 */
	public boolean isTalking() {
		return check("client_is_talker", "1");
	}

	/**
	 * Check if the client is a priority speaker
	 * @return if the client is a priority speaker
	 */
	public boolean isPrioritySpeaker() {
		return check("client_is_priority_speaker", "1");
	}

	//TODO add javadoc
	public String getPhoneticName() {
		return get("client_nickname_phonetic");
	}

	/**
	 * Get the client's description
	 * @return client's description
	 */
	public String getDescription() {
		return get("client_description");
	}

	/**
	 * Check if the client is a channel commander
	 * @return client is a channel commander
	 */
	public boolean isChannelCommander() {
		return check("client_is_channel_commander", "1");
	}

	/**
	 * Get the client's icon ID
	 * @return client's icon ID
	 */
	public int getIconID() {
		return getInt("client_icon_id");
	}

	/**
	 * Get the client's country
	 * @return client's country
	 */
	public String getCountry() {
		return get("client_country");
	}

	/**
	 * Get the client's ID
	 * @return client's ID
	 */
	public int getClientID() {
		return clid;
	}

	private String get(String key) {
		if (!data.containsKey(key))
			throw new NullPointerException(
					"Client doesn't have key or doesn't have permission: " + key + "\nDUMP: " + data);
		return data.get(key);
	}

	private int getInt(String key) {
		return Integer.parseInt(get(key));
	}

	private long getLong(String key) {
		return Long.parseLong(get(key));
	}

	private boolean check(String key, Object value) {
		return get(key).equals(value);
	}

	private String[] getList(String key) {
		return get(key).split(",");
	}

	@Override
	public String toString() {
		return data.toString();
	}

	@Override
	public boolean equals(Object j) {
		if(j instanceof Client) {
			Client c = (Client)j;
			return c.getDatabaseID() == getDatabaseID();
		}
		return false;
	}

	/**
	 * Create a list of clients
	 * 
	 * @param clients
	 *            - {@link Vector} of clients
	 * @return {@link List} of {@link Clients}
	 */
	public static List<Client> createClients(Collection<HashMap<String, String>> clients) {
		return Arrays.asList(createClient(clients.iterator(), new Client[] {}));
	}

	private static Client[] createClient(Iterator<HashMap<String, String>> clients, Client[] list) {
		if (!clients.hasNext())
			return list;
		list = ArrayHelper.append(list, new Client(clients.next()));
		return createClient(clients, list);
	}

	public static List<Client> createAndFilterClients(Collection<HashMap<String, String>> clients,
			Predicate<Client> filter) {
		return Arrays.asList(createClient(clients.iterator(), new Client[] {})).stream().filter(filter)
				.collect(Collectors.toList());
	}
}
