/**
    Copyright (C) 2015  FoxGenesis
 
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.functions;

import java.util.ArrayList;
import java.util.Arrays;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import net.foxgenesis.admin.Client;
import net.foxgenesis.admin.plugin.AbstractFunction;
import net.foxgenesis.admin.plugin.AbstractPlugin;

/**
 * @author Seth
 */
@Function(name = "Bad Nickname", useConfig = true)
public class BadNicknameFunction extends AbstractFunction {

	private boolean enabled;

	public BadNicknameFunction(AbstractPlugin plugin) {
		super(plugin);
	}

	@Override
	public void init(JTS3ServerQuery query) {
		enabled = isEnabled();

		ArrayList<String> regex = new ArrayList<>();
		Arrays.asList(getConfig().getString("banned-names").split(",")).forEach(regex::add);

		getEventManager().addJoinHandler(event -> {
			if (!enabled)
				return;
			Client c = event.getClient();
			for (String a : regex)
				if (c.getNickName().matches(a)) {
					c.kickFromServer("Invalid Nickname");
					break;
				}
		});
	}
}
