/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.functions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Predicate;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import de.stefan1200.jts3serverquery.TS3ServerQueryException;
import net.foxgenesis.admin.Channel;
import net.foxgenesis.admin.Client;
import net.foxgenesis.admin.TeamspeakAdmin;
import net.foxgenesis.admin.commands.ChatCommand;
import net.foxgenesis.admin.plugin.AbstractChatCommand;
import net.foxgenesis.admin.plugin.AbstractFunction;
import net.foxgenesis.admin.plugin.AbstractPlugin;
import net.foxgenesis.util.LoggerUtils;

@Function(name = "Idle Mover", useConfig = true)
public class IdleMoverFunction extends AbstractFunction {

	private JTS3ServerQuery query;
	private long warn, move;
	private String message;
	private boolean enabled = false;
	private Channel channel;
	private ArrayList<Integer> users = new ArrayList<>();
	private Predicate<Client> filter;

	private final Runnable run = () -> {
		try {
			if (!enabled)
				return;
			getClients(client -> client.getIdleTime() >= warn && client.getIdleTime() < move, query).forEach(c -> {
				try {
					if (c.getChannel().getChannelID() != channel.getChannelID()) {
						info(getLOCO().format("warn-message", c.getNickName(),c.getClientID()));
						c.poke(message);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
			getClients(client -> client.getIdleTime() >= move, query).forEach(c -> {
				if (c.getChannel().getChannelID() != channel.getChannelID()) {
					info(getLOCO().format("move-message", c.getNickName(),c.getClientID(),channel.getName()));
					if(!c.moveto(channel))
						LoggerUtils.error(getLOCO().format("no-move",c.getNickName()));
				}
			});
		} catch (TS3ServerQueryException e) {
			e.printStackTrace();
		}
	};

	public IdleMoverFunction(AbstractPlugin plugin) {
		super(plugin);
		registerChatCommand(new IdleMoverChatCommand());
	}

	@Override
	public void init(JTS3ServerQuery query) {
		this.query = query;

		move = getConfig().getLong("idle-time");
		info(getLOCO().format("set-idle-time",move + "ms"));

		warn = getConfig().getLong("warn-time");
		info(getLOCO().format("set-idle-time",warn + "ms"));
		
		long time = getConfig().getLong("update-time");
		info(getLOCO().format("set-idle-time",time + "ms"));

		String[] user = getConfig().getString("users").split(",");
		String[] suser = getConfig().getString("ok-s-users").split(",");
		String[] cuser = getConfig().getString("ok-c-users").split(",");
		ArrayList<Integer> susers = new ArrayList<>(), cusers = new ArrayList<>();
		Arrays.asList(user).forEach(a -> users.add(Integer.parseInt(a)));
		Arrays.asList(suser).forEach(a -> susers.add(Integer.parseInt(a)));
		info(getLOCO().format("excluded-server-groups",susers));
		Arrays.asList(cuser).forEach(a -> cusers.add(Integer.parseInt(a)));
		info(getLOCO().format("excluded-channel-groups",cusers));

		filter = c -> !(c.hasAChannelGroup(cusers) || c.hasAServerGroup(susers));

		message = getConfig().getString("warn-message");
		
		List<Channel> channels = TeamspeakAdmin.getCache().findChannel(getConfig().getString("channel"));
		if(channels.size() == 0) {
			this.error(getLOCO().get("channel-not-found"),true);
			return;
		}
		channel = channels.get(0);
		info(getLOCO().format("set-channel", channel.getName()));
		new Timer("Idle Mover Update Timer").schedule(new TimerTask() {
			@Override
			public void run() {
				run.run();
			}
		}, time, time);
	}

	private List<Client> getClients(Predicate<Client> filter, JTS3ServerQuery query) throws TS3ServerQueryException {
		return TeamspeakAdmin.getCache().filterClients(filter.and(this.filter));
	}

	@ChatCommand(name = "idle", usage = "<enable | disable | status | run>")
	private class IdleMoverChatCommand extends AbstractChatCommand {
		@Override
		public boolean handleCommand(Client client, String[] args, JTS3ServerQuery query) throws Exception {
			if (args != null && args.length == 1)
				switch (args[0]) {
				case "enable":
					enabled = true;
					client.sendMessage("[b]Idle Mover: [/b] Enabled");
					return true;
				case "disable":
					enabled = false;
					client.sendMessage("[b]Idle Mover: [/b] Disabled");
					return true;
				case "status":
					client.sendMessage("[b]Idle Mover: [/b] " + (enabled ? "Enabled" : "Disabled"));
					return true;
				case "run":
					client.sendMessage("Force running idle mover function...");
					run.run();
					return true;
				}
			return false;
		}

		@Override
		public boolean canClientInvoke(Client client) {
			return client.hasAServerGroup(users);
		}
	}
}
