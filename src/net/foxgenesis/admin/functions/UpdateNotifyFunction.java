/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.functions;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Predicate;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import net.foxgenesis.admin.Client;
import net.foxgenesis.admin.TeamspeakAdmin;
import net.foxgenesis.admin.plugin.AbstractFunction;
import net.foxgenesis.admin.plugin.AbstractPlugin;
import net.foxgenesis.util.UpdaterJar;

/**
 * @author Seth
 */
@Function(name = "Update Notify", useConfig = true)
public class UpdateNotifyFunction extends AbstractFunction {

	public UpdateNotifyFunction(AbstractPlugin plugin) {
		super(plugin);
	}

	@Override
	public void init(JTS3ServerQuery query) {
		boolean enabled = isEnabled();
		long time = getConfig().getLong("update-interval");
		info("Update time set to: " + time);

		ArrayList<Integer> users = new ArrayList<>();
		for (String a : getConfig().getString("groups").split(","))
			if (!a.isEmpty())
				users.add(Integer.parseInt(a));
		info("Groups: " + users);

		Predicate<Client> filter = client -> client.hasAServerGroup(users);

		new Timer("Update Notify Timer").scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				if (enabled)
					try {
						verbose("Checking for update...");
						if (UpdaterJar.isUpdateAvailable(TeamspeakAdmin.BETA_VERSION_URL,
								TeamspeakAdmin.VERSION)) {
							verbose("Update available. Notifing groups...");
							TeamspeakAdmin.getCache().filterClients(filter)
									.forEach(client -> client.sendMessage("[b]NEW UPDATE AVAILABLE![/b]"));
						} else
							verbose("Version is up to date");
					} catch (Exception e) {
						e.printStackTrace();
					}
			}
		}, time, time);

		info("Loaded");
	}

}
