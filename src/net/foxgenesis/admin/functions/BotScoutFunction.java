/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.functions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import net.foxgenesis.admin.Client;
import net.foxgenesis.admin.ServerQueryHelper.LogLevel;
import net.foxgenesis.admin.commands.BlackListCommand;
import net.foxgenesis.admin.commands.WhiteListCommand;
import net.foxgenesis.admin.plugin.AbstractFunction;
import net.foxgenesis.admin.plugin.AbstractPlugin;
import net.foxgenesis.util.io.ActiveFileList;
import net.foxgenesis.util.io.IPHelper;

/**
 * @author Seth
 *
 */
@Function(name = "Bot Scout", useConfig = true)
public class BotScoutFunction extends AbstractFunction {

	private final ActiveFileList whitelist = new ActiveFileList(getDataFolder(), "whitelist"),
			blacklist = new ActiveFileList(getDataFolder(), "blacklist");

	public BotScoutFunction(AbstractPlugin plugin) {
		super(plugin);
		ArrayList<Integer> users = new ArrayList<>();
		Arrays.asList(getConfig().getString("users").split(",")).forEach(a -> users.add(Integer.parseInt(a)));

		registerChatCommand(new BlackListCommand(blacklist, this, users));
		registerChatCommand(new WhiteListCommand(whitelist, this, users));
		getEventManager().addJoinHandler(event -> {
			Client client = event.getClient();
			try {
				if (client.isQuery())
					return;
				String ip = client.getIP();
				if (IPHelper.isOnBotDatabase(ip) || (blacklist.matches(ip) && !whitelist.matches(ip))) {
					client.kickFromServer(getConfig().getString("message"));
					info(ip + " - kicked");
					log(LogLevel.INFO, ip + " - kicked");
					writeIP(ip);
				} else
					info(ip + " - connected");
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
	}

	@Override
	public void init(JTS3ServerQuery query) {
		try {
			File file = new File(getDataFolder(),".ips");
			if (!file.exists()) {
				file.createNewFile();
				ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
				out.writeObject(new ArrayList<String>());
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		info("Loaded " + whitelist.getList().size() + " whitelist rules");
		info("Loaded " + blacklist.getList().size() + " blacklist rules");
	}

	@SuppressWarnings("unchecked")
	private List<String> getIPs() throws IOException {
		ObjectInputStream in = new ObjectInputStream(new FileInputStream(new File(getDataFolder(),".ips")));
		List<String> output = null;
		try {
			output = (List<String>) in.readObject();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		in.close();
		return output;
	}

	private void writeIP(String ip) throws IOException {
		if (getIPs().contains(ip))
			return;
		List<String> l = getIPs();
		l.add(ip);
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(new File(getDataFolder(),".ips")));
		out.writeObject(l);
		out.close();
	}

	@Override
	public File getDataFolder() {
		return new File("plugins/" + this.getName().replaceAll(" ", "_"));
	}
}
