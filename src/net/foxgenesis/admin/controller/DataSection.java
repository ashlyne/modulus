/**
    Copyright (C) 2015  FoxGenesis
 
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.controller;

/**
 * @author Seth
 *
 */
public class DataSection {
	private String data;
	
	public DataSection(String title) {
		data = "<div class=\"content section\" id=\"" + title + "\"><h3>" + title + "</h3>";
	}
	
	public DataSection add(String key, Object value) {
		data+="<b>" + key + ":</b> " + value + "<br>";
		return this;
	}
	
	@Override
	public String toString() {
		return data + "</div>";
	}
}
