/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin.controller;

import static net.foxgenesis.util.io.LOCO.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsExchange;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;

import net.foxgenesis.util.LoggerUtils;
import net.foxgenesis.util.io.AHTML;
import net.foxgenesis.util.io.UserDatabase;

/**
 * @author Seth
 *
 */
public abstract class AdminController {
	private static final HashMap<String,ControllerData> handlers = new HashMap<>();
	public static void addHandler(String key, ControllerData handler) {
		if(!handlers.containsKey(key)) {
			LoggerUtils.info(main.get("web-panel"),main.format("contenthandler-add",key));
			handlers.put(key, handler);
		}
	}

	private static final HashMap<String,ControllerSaveable> save = new HashMap<>();
	public static void addSaveHandler(String key, ControllerSaveable handler) {
		if(!save.containsKey(key)) {
			LoggerUtils.info(main.get("web-panel"),main.format("savehandler-add",key));
			save.put(key, handler);
		}
	}

	static {
		AHTML a = new AHTML(AdminController.class.getResourceAsStream("/assets/net/foxgenesis/admin/webpanel/sectors.ahtml"));
		a.foreach((name,h) -> addHandler(name,() -> {return h;}));
	}

	private HttpsServer server;
	private HashMap<String,HttpHandler> map = new HashMap<>();
	private HashMap<String,String> preload = new HashMap<>();
	private HashMap<String,PostHandler> post = new HashMap<>();
	private UserDatabase db;

	public AdminController(InetSocketAddress address, File keystore, char[] password) throws IOException, NoSuchAlgorithmException, KeyStoreException, CertificateException, UnrecoverableKeyException, KeyManagementException {
		db = UserDatabase.get(new File(".users"));
		Runtime.getRuntime().addShutdownHook(new Thread(db::save,"UserDatabase Save Thread"));
		server = HttpsServer.create(address, 0);
		SSLContext c = SSLContext.getInstance("SSL");
		KeyStore ks = KeyStore.getInstance("JKS");
		InputStream ksIs = new FileInputStream(keystore);
		try {
			ks.load(ksIs, password);
		} finally {
			if (ksIs != null) 
				ksIs.close();
		}
		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory
				.getDefaultAlgorithm());
		kmf.init(ks, password);
		c.init(kmf.getKeyManagers(), null, new SecureRandom());
		server.setHttpsConfigurator(new HttpsConfigurator(c) {
			public void configure(HttpsParameters params) {
				try {
					SSLEngine engine = c.createSSLEngine ();
					params.setNeedClientAuth(false);
					params.setCipherSuites(engine.getSupportedCipherSuites());
					params.setProtocols(engine.getSupportedProtocols());
					SSLParameters sslparam = c.getSupportedSSLParameters();
					params.setSSLParameters(sslparam);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		server.setExecutor(java.util.concurrent.Executors.newCachedThreadPool());
		server.createContext("/login",t -> {
			HttpsExchange e = (HttpsExchange)t;
			if(!t.getRequestMethod().equalsIgnoreCase("post")) {
				LoggerUtils.debug(main.get("web-panel"),"Not post! ignoring!");
				finish(t,"",204);
				return;
			}
			if(isUserLoggedIn(t)) {
				finish(t,"accept,reload",200);
				return;
			}
			HashMap<String,String> d = new HashMap<>();
			String[] in = read(t.getRequestBody()).split("&");
			Arrays.asList(in).forEach(s -> {
				String[] a = s.split("=");
				a[1] = a[1].replace('+', ' ');
				d.put(a[0], a[1]);
			});
			try {
				Object j = db.isValid(d.get("username"), d.get("password"));
				if(j != null) {
					e.getSSLSession().putValue("login-name", d.get("username"));
					e.getSSLSession().putValue("ssid", j);
					finish(t,"accept,reload",200);
				} else
					finish(t,"deny,Invalid Login",200);
			} catch (Exception e1) {
				e1.printStackTrace();
				finish(t,"Error while checking login",500);
			}
		});
		server.createContext("/logout",t -> {
			HttpsExchange e = (HttpsExchange)t;
			e.getSSLSession().invalidate();
			c.getServerSessionContext().getSession(e.getSSLSession().getId()).invalidate();
			e.getSSLSession().getSessionContext().setSessionTimeout(1);
			finish(t,"accept",200);
		});
		addPostPage("/user-create",data -> !db.hasEntry(data.get("username")),(name,data) -> {
			try {
				db.createUser(data.get("username"), data.get("password"));
				db.save();
				LoggerUtils.info(main.get("web-panel"),main.format("webpanel-usercreate", data.get("username")));
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	protected void addPage(String path, HttpHandler handler) {
		server.createContext(path,handler);
		map.put(path,handler);
	}

	protected HashMap<String,ControllerData> getSections() {
		return handlers;
	}

	protected HashMap<String,ControllerSaveable> getSaves() {
		return save;
	}

	protected void addPostPage(String path, BiConsumer<String,HashMap<String,String>> data) {
		addPostPage(path,null,data);
	}
	
	protected void addPostPage(String path, Predicate<HashMap<String,String>> check, BiConsumer<String,HashMap<String,String>> data) {
		PostHandler h = new PostHandler(check,data);
		server.createContext(path,h);
		post.put(path, h);
	}

	protected void removePage(String path) {
		map.remove(path);
	}

	protected HashMap<String,HttpHandler> getMap() {
		return map;
	}

	public void start() {
		server.start();
	}

	public void stop(int code) {
		server.stop(code);
	}

	protected void setIndexPage(HttpHandler handler) {
		addPage("/",t -> {
			HttpsExchange e = (HttpsExchange)t;
			if(!isUserLoggedIn(t)) {
				//No need for LOCO
				LoggerUtils.debug(main.get("web-panel"),"Session[" + Arrays.toString(e.getSSLSession().getId()) + "] not logged in. Sending to login page...");
				printPage("assets/net/foxgenesis/admin/webpanel/login.html",e);
			}
			else
				handler.handle(t);
		});
	}

	protected boolean isUserLoggedIn(HttpExchange t) {
		HttpsExchange e = (HttpsExchange)t;
		LoggerUtils.debug(e.getSSLSession().getPeerHost() + " | " + Arrays.toString(e.getSSLSession().getValueNames()));
		return db.isValid(e.getSSLSession().getValue("ssid"));
	}

	protected void preloadPage(String path) {
		if(preload.containsKey(path))
			return;
		LoggerUtils.verbose(main.get("web-panel"),main.format("webpanel-preload",path));
		InputStream i = AdminController.class.getResourceAsStream("/" + path);
		String output = "";
		try (BufferedReader r = new BufferedReader(new InputStreamReader(i,"Cp1252"))) {
			String line = "";
			while((line = r.readLine()) != null) 
				output += line;
		} catch(Exception e) {
			e.printStackTrace();
		}
		preload.put(path, output);
	}

	protected String beforePrint(HttpExchange t, String path, String page, int code){return replace(page);}

	protected void printPage(String path, HttpExchange t) throws IOException {
		if(preload.containsKey(path)) {
			finish(t,preload.get(path),200);
			return;
		}
		InputStream i = AdminController.class.getResourceAsStream("/" + path);
		if(i == null) {
			LoggerUtils.error(main.get("web-panel"),main.format("webpanel-nopage",path));
			finish(t,"<h1>404</h1>",404);
			return;
		}
		String output = "";
		try (BufferedReader r = new BufferedReader(new InputStreamReader(i,"Cp1252"))) {
			String line = "";
			while((line = r.readLine()) != null) 
				output += line;
		} catch(Exception e) {
			e.printStackTrace();
		}
		finish(t,output,200);
	}

	protected void printPageIfLoggedIn(String path, HttpExchange t) throws IOException {
		HttpsExchange e = (HttpsExchange)t;
		if(!isUserLoggedIn(t)) {
			// No need for LOCO
			LoggerUtils.debug(main.get("web-panel"),"Session[" + Arrays.toString(e.getSSLSession().getId()) + "] not logged in. Sending to login page...");
			printPage("assets/net/foxgenesis/admin/webpanel/login.html",e);
		} else printPage(path,t);
	}

	protected void finish(HttpExchange t, String page, int code) throws IOException {
		page = beforePrint(t,t.getRequestURI().toString(),page,code);
		t.sendResponseHeaders(code, page.length());
		if(code == 200) {
			OutputStream os = t.getResponseBody();
			os.write(page.getBytes());
			os.close();
		}
		t.close();
	}

	protected void addAsset(String type, String path, String filepath) {
		addAsset(type,path,filepath,true);
	}

	protected void addAsset(String type, String path, String filepath, boolean preload) {
		server.createContext(path,new AssetHandler(type,filepath,preload));
	}

	private class AssetHandler implements HttpHandler {
		private final String type,path;
		private volatile byte[] data = null;
		private final boolean preload;

		public AssetHandler(String type, String path, boolean preload) {
			this.type = type;
			this.path = "/" + path;
			this.preload = preload;
			if(preload)
				data = read(AdminController.class.getResourceAsStream(this.path)).getBytes();
		}

		@Override
		public void handle(HttpExchange t) throws IOException {
			if(!preload)
				data = read(AdminController.class.getResourceAsStream(this.path)).getBytes();
			t.getResponseHeaders().add("type", type);
			t.sendResponseHeaders(200, data.length);
			t.getResponseBody().write(data);
			t.getResponseBody().close();
		}
	}

	private class PostHandler implements HttpHandler {
		private BiConsumer<String, HashMap<String, String>> onSave;
		private Predicate<HashMap<String,String>> check;
		
		public PostHandler(Predicate<HashMap<String,String>> check, BiConsumer<String, HashMap<String, String>> data) {
			this.onSave = data;
			if(check == null)
				check = d -> true;
			this.check = check;
		}

		@Override
		public void handle(HttpExchange t) throws IOException {
			if(!t.getRequestMethod().equalsIgnoreCase("post")) {
				// No need for LOCO
				LoggerUtils.debug(main.get("web-panel"),"Not post! ignoring!");
				finish(t, "not post", 403);
				return;
			}
			if(!isUserLoggedIn(t)) {
				finish(t,"deny",403);
				return;
			}
			HashMap<String,String> d = new HashMap<>();
			String[] in = read(t.getRequestBody()).split("&");
			Arrays.asList(in).forEach(s -> {
				//TODO fix space issues
				String[] a = s.split("=");
				a[1] = a[1].replace('+', ' ');
				d.put(a[0], a[1]);
			});
			String w = d.get("config");
			d.remove("config");
			if(check.test(d)) {
				//onSave.accept(w, d);
				finish(t,"accept",200);
			}
			else
				finish(t,"deny",200);
		}
	}

	private static String read(InputStream i) {
		if(i == null) {
			// No need for LOCO
			LoggerUtils.error(main.get("web-panel"),"Input stream is null!");
			return null;
		}
		String output = "";
		try (BufferedReader r = new BufferedReader(new InputStreamReader(i))) {
			String line = "";
			while((line = r.readLine()) != null) 
				output += line;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return output;
	}

	protected void redirect(String to, HttpExchange t, int time) throws IOException {
		finish(t,"<html><head><meta http-equiv=\"refresh\" content=\"" + time + "; url=" + to + "\"/></head></html>",200);
	}

	private static String replace(String page) {
		int start = page.indexOf("<%");
		if(start == -1)
			return page;
		int end = page.indexOf("%>", start)+2;
		if(end == -1)
			return page;
		String handle = page.substring(start+2,end-2);
		String output = main.format("webpanel-no-controller", handle);
		if(handlers.containsKey(handle.toLowerCase()))
			page = insert(page,start,end,handlers.get(handle.toLowerCase()).getHTMLData());
		else {
			page = insert(page,start,end,"<div><b style=\"color:red\">" + output + "</b></div>");
			LoggerUtils.error(main.get("web-panel"),output);
		}
		return replace(page);
	}

	private static String insert(String a, int start, int end, String b) {
		String s = a.substring(0,start);
		String e = a.substring(end);
		return s + b + e;
	}
}
