/**
    Copyright (C) 2015  FoxGenesis
 
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.admin;

/**
 * All keys for channel edit
 * 
 * @author Seth
 */
public enum ChannelProperties {
	/**
	 * Name of the channel
	 */
	CHANNEL_NAME, 
	/**
	 * Topic of the channel
	 */
	CHANNEL_TOPIC, 
	/**
	 * Description of the channel
	 */
	CHANNEL_DESCRIPTION, 
	/**
	 * Password of the channel
	 */
	CHANNEL_PASSWORD,
	/**
	 * Codec used by the channel
	 */
	CHANNEL_CODEC,
	/**
	 * Codec quality used by the channel
	 */
	CHANNEL_CODEC_QUALITY,
	/**
	 * Individual max number of clients for the channel
	 */
	CHANNEL_MAXCLIENTS,
	/**
	 * Individual max number of clients for the channel family
	 */
	CHANNEL_MAXFAMILYCLIENTS,
	/**
	 * ID of the channel below which the channel is positioned
	 */
	CHANNEL_ORDER,
	/**
	 * Indicates whether the channel is permanent or not
	 */
	CHANNEL_FLAG_PERMANENT,
	/**
	 * Indicates whether the channel is semi-permanent or not
	 */
	CHANNEL_FLAG_SEMI_PERMANENT,
	/**
	 * Indicates whether the channel is temporary or not
	 */
	CHANNEL_FLAG_TEMPORARY,
	/**
	 * Indicates whether the channel is the virtual servers default channel or not
	 */
	CHANNEL_FLAG_DEFAULT,
	/**
	 * Indicates whether the channel has a max clients limit or not
	 */
	CHANNEL_FLAG_MAXCLIENTS_UNLIMITED,
	/**
	 * Indicates whether the channel has a max family clients limit or not
	 */
	CHANNEL_FLAG_MAXFAMILYCLIENTS_UNLIMITED,
	/**
	 * Indicates whether the channel inherits the max family clients from his parent channel or not
	 */
	CHANNEL_FLAG_MAXFAMILYCLIENTS_INHERITED,
	/**
	 * Needed talk power for this channel
	 */
	CHANNEL_NEEDED_TALK_POWER,
	/**
	 * Phonetic name of the channel
	 */
	CHANNEL_NAME_PHONETIC,
	/**
	 * CRC32 checksum of the channel icon
	 */
	CHANNEL_ICON_ID,
	/**
	 * Indicates whether speech data transmitted in this channel is encrypted or not
	 */
	CHANNEL_CODEC_IS_UNENCRYPTED,
	/**
	 * The channels parent ID
	 */
	CPID;
}
