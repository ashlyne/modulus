package net.foxgenesis.util;

public class Version {
	private final int version,major,minor;

	public Version(String version) {
		String[] d = version.split("\\.");
		if(d.length > 3 || d.length == 0)
			throw new NumberFormatException("Too many/little arguments for version: " + version);
		this.version = Integer.parseInt(d[0]);
		if(d.length > 1)
			major = Integer.parseInt(d[1]);
		else
			major = 0;
		if(d.length > 2)
			minor = Integer.parseInt(d[2]);
		else
			minor = 0;
	}

	public Version(int version, int major, int minor) {
		this.version = version;
		this.major = major;
		this.minor = minor;
	}

	public int getVersion() {
		return version;
	}

	public int getMajor() {
		return major;
	}

	public int getMinor() {
		return minor;
	}

	@Override
	public String toString() {
		return version + "." + major + "." + minor;
	}

	@Override
	public boolean equals(Object j) {
		if(j instanceof Version) {
			Version v = (Version)j;
			return v.getVersion() == version && 
					v.getMajor() == major &&
					v.getMinor() == minor;
		}
		return false;
	}

	public boolean isGreaterThan(Version v) {
		if(this.equals(v))
			return false;
		else if(version < v.getVersion())
			return false;
		else if(version > v.getVersion())
			return true;
		else if(major > v.getMajor())
			return true;
		else if(major < v.getMajor())
			return false;
		else if(minor > v.getMinor())
			return true;
		return false;
	}

	public boolean isLessThan(Version v) {
		if(this.equals(v))
			return false;
		else if(version < v.getVersion())
			return true;
		else if(version > v.getVersion())
			return false;
		else if(major > v.getMajor())
			return false;
		else if(major < v.getMajor())
			return true;
		else if(minor > v.getMinor())
			return false;
		else if(minor < v.getMinor())
			return true;
		return false;
	}
}
