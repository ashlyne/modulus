/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.function.Supplier;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import sun.awt.OSInfo.OSType;

/**
 * @author Seth
 *
 */
public final class UpdaterJar {
	private static JFrame frame;
	private static JProgressBar bar;
	private static JTextArea area;
	private static boolean headless = false;

	/**
	 * Must be called as a jar file
	 * @param args
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws IOException, URISyntaxException, InterruptedException {
		try {
			if (args.length < 2) {
				System.err.println("Not Enough Arguments\n");
				System.out.println("Usage: <file to update> <update url>");
				System.out.println("Arguments: ");
				System.out.println("autostart\tAuto runs the jar file after update");
				return;
			}
			boolean autostart = args.length >= 3 && args[args.length - 1].equalsIgnoreCase("autostart");
			String fileName = UpdaterJar.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			headless = System.console() != null;
			if (!headless) {
				frame = new JFrame("Updater");
				bar = new JProgressBar();
				area = new JTextArea();
				frame.setSize(500, 300);
				frame.setResizable(false);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				area.setEditable(false);
				Dimension size = new Dimension(500, 300);
				area.setPreferredSize(size);
				area.setLineWrap(true);
				bar.setForeground(Color.GREEN);
				JScrollPane pane = new JScrollPane(area, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
						JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
				pane.setAutoscrolls(true);
				pane.setPreferredSize(size);
				frame.add(bar, BorderLayout.PAGE_END);
				frame.add(pane, BorderLayout.CENTER);
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}

			File toDelete = new File(args[0]);
			out("Finding old version...");
			if (!toDelete.exists()) {
				if (headless) {
					System.err.println("Failed to find old version!");
					Thread.sleep(1000);
				} else
					JOptionPane.showMessageDialog(frame, "Failed to find old version");
				System.exit(0);
			} else {
				out("Deleting old version...");
				doUntil(toDelete::delete,10);
			}
			out("Old version deleted");
			URL toDownload = new URL(args[1]);
			out("Creating input stream from download location...");
			FileOutputStream out = new FileOutputStream(toDelete);
			InputStream in = toDownload.openStream();
			out("Stream opened");
			out("downloading new version; " + in.available() + " bytes");
			copy(in, out, true);
			out("Done");
			if (!headless) {
				if (!autostart)
					JOptionPane.showMessageDialog(frame, "Update Complete!\nPlease launch application again.");
				frame.dispose();
				if (autostart)
					Runtime.getRuntime().exec(
							new String[] { "cmd.exe", "/c", "java", "-jar", toDelete.toString(), "Del!" + fileName });
			} else {
				if (!autostart)
					System.out.println("Update Complete!");
				else
					Runtime.getRuntime().exec(new String[] { "java", "-jar", toDelete.toString(), "Del!" + fileName });
			}
			System.exit(0);
		} catch (Exception e) {
			if (headless)
				e.printStackTrace();
			else
				JOptionPane.showMessageDialog(frame, e.getMessage());
		}
	}

	private static void out(Object msg) {
		if(headless)
			System.out.println(msg);
		else
			area.append(msg + "\n");
	}

	private static long copy(InputStream from, OutputStream to, boolean download) throws IOException {
		if (from == null || to == null) {
			if (headless)
				System.err.println("Failed to find input or output stream!");
			else
				JOptionPane.showMessageDialog(frame, "Failed to find input or output stream");
			return -1;
		}
		byte[] buf = new byte[8000];
		long total = 0;
		if (!headless && download)
			bar.setMaximum(from.available());
		while (true) {
			int r = from.read(buf);
			if (r == -1)
				break;
			to.write(buf, 0, r);
			total += r;
			if (!headless && download)
				bar.setValue((int) total);
			if (download)
				out("Downloaded: " + r + " bytes");
		}
		return total;
	}
	
	public static void run(Version version, String versionURL, String jarURL, String[] args, boolean autoRun) {
		System.out.println("Checking for update...");
		OSType os = sun.awt.OSInfo.getOSType();
		try {
			if (isUpdateAvailable(versionURL, version))
				startUpdate(jarURL, os, autoRun);
			else
				cleanUp(args);
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Failed to check for update");
			cleanUp(args);
		}
	}

	private static void startUpdate(String updateURL, OSType os, boolean autoRun) throws IOException {
		if (headless) {
			System.out.println("Update available!");
			String answer = "";
			do {
				System.out.println("Would you like to update? [y/n]: ");
				answer = System.console().readLine();
			} while (!(answer.equalsIgnoreCase("y") || answer.equalsIgnoreCase("n")));
			if (!answer.equalsIgnoreCase("y"))
				return;
		} else {
			int option = JOptionPane.showConfirmDialog(null, "Would you like to update?", "Update Available!",
					JOptionPane.YES_NO_OPTION);
			if (option != JOptionPane.OK_OPTION)
				return;
		}
		File f = new File("Updater.jar");
		if(f.exists())
			f.delete();
		f.createNewFile();
		copy(UpdaterJar.class.getResourceAsStream("/Updater.jar"),new FileOutputStream(f),false);
		String fileName = new File(UpdaterJar.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getName();
		Runtime.getRuntime().exec(new String[]{"java","-jar",f.toString(),fileName,updateURL,""+autoRun});
		System.exit(0);
	}
	
	public static void cleanUp(String[] args) {
		System.out.println("Running update clean up...");
		File file = null;
		if (args != null)
			for (String a : args)
				if (a.startsWith("Del!"))
					file = new File(a.split("!", 2)[1]);
		if (file == null)
			file = new File("Updater.jar");
		if (file.exists()) {
			System.out.println("Deleting updater...");
			int count = 0;
			do {
				file.delete();
				count++;
			} while(count <= 10 && file.exists());
		}
		System.out.println("Clean up completed");
	}

	public static boolean isUpdateAvailable(String updateURL, Version version) throws IOException {
		Version v = getCurrentVersion(updateURL);
		System.out.println("Server Version: " + v);
		System.out.println("Client Version: " + version);
		return getCurrentVersion(updateURL).isGreaterThan(version);
	}

	public static Version getCurrentVersion(String updateURL) throws IOException {
		return new Version(getHTML(new URL(updateURL)));
	}

	private static String getHTML(URL url) throws IOException {
		BufferedReader in = getStream(url);
		String output = "";
		String inputLine;
		while ((inputLine = in.readLine()) != null)
			output += inputLine;
		in.close();
		return output;
	}

	private static BufferedReader getStream(URL url) throws IOException {
		URLConnection connection = url.openConnection();
		connection.setRequestProperty("User-Agent", "X-Updater");
		connection.setReadTimeout(5000);
		return new BufferedReader(new InputStreamReader(connection.getInputStream()));
	}
	
	private static void doUntil(Supplier<Boolean> d, int times) {
		int count = 0;
		boolean go = true;
		do {
			go = d.get();
			count++;
		} while(go && count <= times);
	}
}
