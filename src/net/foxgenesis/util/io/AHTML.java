/**
    Copyright (C) 2015  FoxGenesis
 
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.util.io;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.function.BiConsumer;

import net.foxgenesis.util.LoggerUtils;

/**
 * @author Seth
 *
 */
public class AHTML {
	private final HashMap<String,String> map = new HashMap<>();
	
	public AHTML(InputStream in) {
		String a = read(in);
		Arrays.asList(a.split("~")).forEach(s -> {
			String n = s.substring(0,s.indexOf('|'));
			String n1 = s.substring(s.indexOf('|') + 1);
			LoggerUtils.verbose("AHTML", "Found AHTML for " + n);
			map.put(n, n1);
		});
	}
	
	public String get(String key) {
		return map.get(key);
	}
	
	public boolean hasKey(String key) {
		return map.containsKey(key);
	}
	
	public void foreach(BiConsumer<String,String> action) {
		map.forEach(action);
	}
	
	private static String read(InputStream i) {
		if(i == null) {
			LoggerUtils.error("AHTML","Input stream is null!");
			return null;
		}
		String output = "";
		try (BufferedReader r = new BufferedReader(new InputStreamReader(i))) {
			String line = "";
			while((line = r.readLine()) != null) 
				output += line;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return output;
	}
}
