/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.util.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedHashMap;

import net.foxgenesis.admin.plugin.AbstractPlugin;
import net.foxgenesis.util.LoggerUtils;

/**
 * @author Seth
 */
public class LOCO {
	public static final String COUNTRY = System.getProperty("user.country"); 
	public static final String LANGUAGE = System.getProperty("user.language");
	private static final String local = "lang/" + LANGUAGE + "_" + COUNTRY + ".lang";
	
	/**
	 * This is the main localization of the program. No need to use.
	 */
	public static final LOCO main = new LOCO();
	
	/**
	 * Called by a plugin on load. This get the localization file if there is one
	 * @param c - AbstractPlugin class
	 * @return Localization
	 */
	public static LOCO getLOCO(Class<?> c) {
		return new LOCO(c.getResourceAsStream("/" + local));
	}
	
	private final HashMap<String, String> data = new LinkedHashMap<>();
	
	private LOCO() {
		this(LOCO.class.getResourceAsStream("/" + local));
	}
	
	private LOCO(InputStream in) {
		if(in == null)
			throw new NullPointerException("Unable to find LOCO");
		try(BufferedReader r = new BufferedReader(new InputStreamReader(in))) {
			String line = "";
			while((line = r.readLine()) != null)
				if(!line.trim().startsWith("#") && !line.isEmpty())
					add(line);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public LOCO(AbstractPlugin p) {
		this(p.getResource(local));
	}

	private void add(String line) {
		line = line.trim();
		String[] d = line.split(":", 2);
		if (d.length == 2) {
			data.put(d[0].trim(), d[1].trim());
		} else
			LoggerUtils.warning("Invalid line in configuration: " + line);
	}

	/**
	 * Get a {@link String} from a given key
	 * 
	 * @param key
	 *            - key to use
	 * @return Wanted data as a {@link String}
	 * @throws NullPointerException
	 *             if data was not found
	 */
	public String get(String key) {
		if (!containsKey(key))
			throw new NullPointerException("No value with key: " + key);
		return data.get(key);
	}
	
	public String format(String key, Object... args) {
		return String.format(get(key), args);
	}

	/**
	 * Checks whether the configuration file has a key
	 * 
	 * @param key
	 *            - key to check
	 * @return configuration file has key
	 */
	public boolean containsKey(String key) {
		return data.containsKey(key);
	}
}
