/**
    Copyright (C) 2015  FoxGenesis
 
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.util.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/**
 * File that is read as a list, line by line. This list is read every time data
 * is wanted. Thus, ActiveFileList.
 * 
 * @author Seth
 */
public class ActiveFileList {
	private final File f;

	/**
	 * Create a new ActiveFileList with a given name
	 * 
	 * @param name
	 *            - file name
	 */
	public ActiveFileList(String name) {
		this(null, name);
	}

	/**
	 * Create a new ActiveFileList with in a given folder and with a given name
	 * 
	 * @param parent
	 *            - folder to use
	 * @param name
	 *            - name of file
	 */
	public ActiveFileList(File parent, String name) {
		f = new File(parent, name);
		if (!f.exists())
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	/**
	 * Reads the list from the file
	 * 
	 * @return the list
	 */
	public List<String> getList() {
		try {
			return Files.readAllLines(f.toPath());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Appends to the file
	 * 
	 * @param msg
	 *            - String to write
	 */
	public void append(String msg) {
		try (BufferedWriter w = new BufferedWriter(new FileWriter(f.toString(), true))) {
			w.write(msg);
			w.newLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Removes a line from the list. This will rewrite the file.
	 * 
	 * @param s
	 *            - line to remove
	 */
	public void remove(String s) {
		List<String> st = getList();
		st.remove(s);
		rewrite(st);
	}

	/**
	 * Removes a line based on its index. This will rewrite the file
	 * 
	 * @param index
	 *            - index of line
	 */
	public void remove(int index) {
		List<String> st = getList();
		st.remove(index);
		rewrite(st);
	}

	private void rewrite(List<String> st) {
		try (BufferedWriter w = new BufferedWriter(new FileWriter(f.toString(), false))) {
			st.forEach(sd -> {
				try {
					w.write(sd);
					w.newLine();
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Useful only if the file is a list of regex. Checks against the list of
	 * regex with a String.
	 * 
	 * @param string
	 *            - string to use
	 * @return if the string matched any regex
	 */
	public boolean regexTest(String string) {
		return getList().stream().anyMatch(regex -> string.matches(regex));
	}

	/**
	 * Checks against each line with regex
	 * 
	 * @param regex
	 *            - regex to use
	 * @return any line matches regex provided
	 * @see String.matches
	 */
	public boolean matches(String regex) {
		return getList().stream().anyMatch(line -> line.matches(regex));
	}
}
