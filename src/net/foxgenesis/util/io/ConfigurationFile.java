/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.util.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Consumer;

import net.foxgenesis.admin.plugin.AbstractFunction;
import net.foxgenesis.admin.plugin.TeamspeakAdminController;
import net.foxgenesis.util.LoggerUtils;
import net.foxgenesis.util.StringHelper;

/**
 * Basic configuration file setup[ {@code key: value}]. Lines that start with
 * <i>#</i> will not be parsed. Class used by {@link AbstractFunction} for
 * function configuration and the main configuration file of the bot.
 * 
 * @author Seth
 */
public class ConfigurationFile {
	private final File f;
	private final HashMap<String, String> data = new LinkedHashMap<>();
	private Consumer<ConfigurationFile> onUpdate;

	/**
	 * This constructor is mainly used for {@link AbstractFunction}'s
	 * configuration
	 * 
	 * @param loader
	 *            - class loader of the function
	 * @param parent
	 *            - data folder of the plugin
	 * @param resource
	 *            - name of the resource to copy over
	 */
	public ConfigurationFile(ClassLoader loader, File parent, String resource) {
		this.f = loadResource(loader, parent, resource, loader != null);
		try {
			Files.readAllLines(f.toPath()).stream().filter(line -> !line.trim().startsWith("#") && !line.isEmpty())
			.forEach(this::add);
			setController();
		} catch (IOException e) {
			LoggerUtils.error("Error while reading configuration file " + e.getMessage());
		}
	}

	/**
	 * Find and extract a configuration file to the root folder
	 * 
	 * @param resource
	 *            - name of the resource to extract
	 * @see ClassLoader.getResourceAsStream
	 */
	public ConfigurationFile(InputStream stream, File f) {
		this.f = loadResource(stream, f);
		try {
			Files.readAllLines(f.toPath()).stream().filter(line -> !line.trim().startsWith("#") && !line.isEmpty())
			.forEach(this::add);
			setController();
		} catch (IOException e) {
			LoggerUtils.error("Error while reading configuration file " + e.getMessage());
		}
	}

	/**
	 * Read a configuration from an already existing file
	 * 
	 * @param file
	 *            - {@link File} to use
	 */
	public ConfigurationFile(File f) {
		this.f = f;
		try {
			Files.readAllLines(f.toPath()).stream().filter(line -> !line.trim().startsWith("#") && !line.isEmpty())
			.forEach(this::add);
			setController();
		} catch (IOException e) {
			LoggerUtils.error("Error while reading configuration file" + e.getMessage());
		}
	}

	private void setController() {
		TeamspeakAdminController.addSaveHandler(f.getName(), newData -> {
			LoggerUtils.debug("Save[" + f.getName() + "]: " + newData);
			data.clear();
			data.putAll(newData);
			try {
				save();
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(onUpdate != null)
				onUpdate.accept(this);
		});
		TeamspeakAdminController.addHandler(f.getName(), () -> {
			String output = "<div class=\"content section config-file\"><form action=\"save-config\" method=\"post\"><table><th>Configuration File:</th>";
			for(Entry<String, String> a: data.entrySet())
				output+= "<tr><td><b>" + a.getKey() + ": </b></td><td><input type=\"" + 
						(a.getKey().equalsIgnoreCase("password")?"password":"text") + "\" name=\"" + 
						a.getKey() + "\" value=\"" + a.getValue() + "\"></td></tr>";
			output+="<tr><td><input type=\"hidden\" name=\"config\" value=\"" + f.getName() + "\"><input type=\"submit\" value=\"Save\"></td></tr>";
			return output + "</table></form></div>";
		});
	}
	
	private void save() throws IOException {
		List<String> l = Files.readAllLines(f.toPath());
		try(BufferedWriter w = new BufferedWriter(new FileWriter(f))) {
			l.forEach(line -> {
				try {
					if(line.trim().startsWith("#") || line.isEmpty()) {
						w.write(line + System.lineSeparator());
					} else {
						line = line.trim();
						String[] d = line.split(":", 2);
						if(data.containsKey(d[0]))
							w.write(d[0] + ":" + data.get(d[0]) + System.lineSeparator());
					}
				} catch(IOException e) {
					e.printStackTrace();
				}
			});
		}
	}

	private void add(String line) {
		line = line.trim();
		String[] d = line.split(":", 2);
		if (d.length == 2) {
			data.put(d[0].trim(), d[1].trim());
		} else
			LoggerUtils.warning("Invalid line in configuration: " + line);
	}

	public void onUpdate(Consumer<ConfigurationFile> config) {
		this.onUpdate = config;
	}

	/**
	 * Get an {@link Integer} from a given key
	 * 
	 * @param key
	 *            - key to use
	 * @return Wanted data as an {@link Integer}
	 * @throws NullPointerException
	 *             if data was not found
	 */
	public int getInt(String key) {
		return Integer.parseInt(StringHelper.removeNonNumbers(getString(key)));
	}

	/**
	 * Get a {@link Double} from a given key
	 * 
	 * @param key
	 *            - key to use
	 * @return Wanted data as a {@link Double}
	 * @throws NullPointerException
	 *             if data was not found
	 */
	public double getDouble(String key) {
		return Double.parseDouble(StringHelper.removeNonNumbers(getString(key)));
	}

	/**
	 * Get a {@link Long} from a given key
	 * 
	 * @param key
	 *            - key to use
	 * @return Wanted data as a {@link Long}
	 * @throws NullPointerException
	 *             if data was not found
	 */
	public long getLong(String key) {
		return Long.parseLong(StringHelper.removeNonNumbers(getString(key)));
	}

	/**
	 * Get a {@link Float} from a given key
	 * 
	 * @param key
	 *            - key to use
	 * @return Wanted data as a {@link Float}
	 * @throws NullPointerException
	 *             if data was not found
	 */
	public float getFloat(String key) {
		return Float.parseFloat(StringHelper.removeNonNumbers(getString(key)));
	}

	/**
	 * Get a {@link Boolean} from a given key
	 * 
	 * @param key
	 *            - key to use
	 * @return Wanted data as a {@link Boolean}
	 * @throws NullPointerException
	 *             if data was not found
	 */
	public boolean getBoolean(String key) {
		return Boolean.parseBoolean(getString(key));
	}

	/**
	 * Get a {@link String} from a given key
	 * 
	 * @param key
	 *            - key to use
	 * @return Wanted data as a {@link String}
	 * @throws NullPointerException
	 *             if data was not found
	 */
	public String getString(String key) {
		if (!containsKey(key))
			throw new NullPointerException("No value with key: " + key);
		return data.get(key);
	}

	/**
	 * Gets a {@link List} of {@link String}s from a given key
	 * @param key - key to use
	 * @return wanted data as a {@link List}
	 * @throws NullPointerException if data was not found
	 */
	public List<String> getStringList(String key) {
		return Arrays.asList(getString(key).split(","));
	}

	/**
	 * Gets a {@link List} of {@link Integer}s from a given key
	 * @param key - key to use
	 * @return wanted data as a {@link List}
	 * @throws NullPointerException if data was not found
	 */
	public List<Integer> getIntegerList(String key) {
		String[] d = getString(key).split(",");
		ArrayList<Integer> out = new ArrayList<>();
		for(String a: d)
			try {
				out.add(Integer.parseInt(StringHelper.removeNonNumbers(a)));
			} catch(Exception e){}
		return out;
	}

	/**
	 * Checks whether the configuration file has a key
	 * 
	 * @param key
	 *            - key to check
	 * @return configuration file has key
	 */
	public boolean containsKey(String key) {
		return data.containsKey(key);
	}

	private File loadResource(ClassLoader loader, File parent, String resource, boolean useFolder) {
		if (parent == null)
			parent = new File("").getParentFile();
		File folder = new File(parent, "cfg");
		if (useFolder && !folder.exists())
			folder.mkdir();
		else {
			File f = new File(useFolder ? folder + resource : resource);
			if (f.exists())
				return f;
		}
		File resourceFile = new File(useFolder ? folder : parent, resource);
		try {
			if (!resourceFile.exists()) {
				resourceFile.createNewFile();
				if (loader != null)
					try (InputStream in = loader.getResourceAsStream(resource);
							OutputStream out = new FileOutputStream(resourceFile)) {
						StreamHelper.copy(in, out);
					}
				else
					try (InputStream in = getClass().getResourceAsStream(resource);
							OutputStream out = new FileOutputStream("/" + resourceFile)) {
						StreamHelper.copy(in, out);
					}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resourceFile;
	}

	private File loadResource(InputStream stream, File f) {
		if (!f.exists()) {
			try {
				if (stream == null) {
					System.err.println("InputStream is null!");
					System.exit(1);
				}
				if (f.getParentFile() != null)
					f.getParentFile().mkdirs();
				f.createNewFile();
				try (OutputStream out = new FileOutputStream(f)) {
					StreamHelper.copy(stream, out);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return f;
	}
}
