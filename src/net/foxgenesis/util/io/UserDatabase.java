/**
    Copyright (C) 2015  FoxGenesis
 
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.util.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.HashMap;

import net.foxgenesis.util.LoggerUtils;
import net.foxgenesis.util.PES;

/**
 * @author Seth
 */
public final class UserDatabase implements Serializable {
	private static final long serialVersionUID = -2873000071150244974L;
	private HashMap<String,UserEntry> map = new HashMap<>();
	private File f;
	
	private UserDatabase(File f) {
		this.f = f;
	}
	
	public boolean createUser(String username, String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
		if(map.containsKey(username))
			return false;
		byte[] salt = PES.generateSalt();
		byte[] ep = PES.getEncryptedPassword(password, salt);
		map.put(username, new UserEntry(salt,ep));
		return true;
	}
	
	public Object isValid(String username,String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
		if(!map.containsKey(username))
			return null;
		UserEntry e = map.get(username);
		if(PES.authenticate(password, e.getEP(), e.getSalt()))
			return e;
		return null;
	}
		
	public boolean isValid(Object a) {
		if(a == null) {
			LoggerUtils.debug("[Users Database]: Object is null!");
			return false;
		} else if(!(a instanceof UserEntry)) {
			LoggerUtils.debug("[Users Database]: Object must be UserEntry!");
			return false;
		}
		try {
			UserEntry e = (UserEntry)a;
			return map.containsValue(e);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean hasEntry(String key) {
		return map.containsKey(key);
	}
	
	private class UserEntry implements Serializable {
		private static final long serialVersionUID = 2957976219736409878L;
		private final byte[] salt,p;
		
		public UserEntry(byte[] salt, byte[] p) {
			this.salt = salt;
			this.p = p;
		}
		
		public byte[] getSalt() {
			return salt;
		}
		
		public byte[] getEP() {
			return p;
		}
		
		@Override
		public boolean equals(Object j) {
			if(j instanceof UserEntry) {
				UserEntry e = (UserEntry)j;
				return Arrays.equals(e.getSalt(), getSalt()) &&
						Arrays.equals(e.getEP(), getEP());
			}
			return false;
		}
	}
	
	public void save() {
		if(f == null)
			throw new NullPointerException("File not found for database!");
		save(f,this);
	}
	
	public static UserDatabase get(File f) throws IOException {
		if(f.exists()) {
			Object j = readFile(f);
			if(j instanceof UserDatabase)
				return (UserDatabase)j;
			else
				throw new UnsupportedEncodingException("File is not of type UserDatabase!");
		}
		if(f.createNewFile()) {
			UserDatabase db = new UserDatabase(f);
			db.save();
			return db;
		} else {
			LoggerUtils.error("Failed to create file");
			return null;
		}
	}
	
	private static Object readFile(File f) {
		Object out = null;
		try(ObjectInputStream in = new ObjectInputStream(new FileInputStream(f))) {
			out = in.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return out;
	}
	
	private static void save(File f, UserDatabase db) {
		try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(f))) {
			out.writeObject(db);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
