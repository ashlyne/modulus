/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.util.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import net.foxgenesis.util.LoggerUtils;

/**
 * Class to help with IPs
 * 
 * @author Seth
 */
public final class IPHelper {

	/**
	 * Check if an IP is on the BotScout database
	 * 
	 * @param ip
	 *            - IP to use
	 * @return if the IP is on the database
	 * @throws IOException
	 *             shit happens
	 */
	public static boolean isOnBotDatabase(String ip) throws IOException {
		URLConnection connection = new URL("http://botscout.com/test/?ip=" + ip + "&key=GBKwMdyZ6fWcXg0")
				.openConnection();
		connection.setRequestProperty("User-Agent", "X-ProxyChecker");
		connection.setReadTimeout(5000);
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		return in.lines().anyMatch(line -> line.contains("Y"));
	}

	/**
	 * Get a WhoIs for an IP
	 * 
	 * @param hostIp
	 *            - IP to use
	 * @return WhoIs data from given IP
	 */
	public static HashMap<String, String> whois(String hostIp) {
		HashMap<String, String> data = new HashMap<>();
		try {
			getPre(new URL("https://who.is/whois-ip/ip-address/" + hostIp)).stream()
					.filter(line -> !line.trim().startsWith("%") && !line.trim().isEmpty()).forEach(line -> {
						String[] s = line.split(":", 2);
						data.put(s[0].trim(), s[1].trim());
					});
		} catch (IOException e) {
			LoggerUtils.error(e.getMessage());
		}
		return data;
	}

	private static List<String> getPre(URL url) {
		try {
			List<String> d = Arrays.asList(SiteReader.getHTMLLines(url));
			return d.subList(d.indexOf("[whois.ripe.net]") + 1, d.indexOf(
					"% This query was served by the RIPE Database Query Service version 1.83-JAVA8 (DB-3)</pre>"));
		} catch (IOException e) {
			LoggerUtils.error(e.getMessage());
		}
		return null;
	}

	/**
	 * Check if a port is open on an IP
	 * 
	 * @param ip
	 *            - IP to use
	 * @param port
	 *            - port to check
	 * @param timeout
	 *            - timeout of operation
	 * @return if the port is open
	 */
	public static boolean portIsOpen(String ip, int port, int timeout) {
		try {
			Socket socket = new Socket();
			socket.connect(new InetSocketAddress(ip, port), timeout);
			socket.close();
			return true;
		} catch (Exception ex) {
			return false;
		}
	}
}
