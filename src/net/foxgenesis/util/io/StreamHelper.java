/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.util.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Class to help with data streams
 * 
 * @author Seth
 */
public final class StreamHelper {
	private static final int BUF_SIZE = 0x1000; // 4K

	/**
	 * From the GoogleIO Commons. Copy an input stream to and output stream
	 * 
	 * @param from
	 *            - input stream
	 * @param to
	 *            - output stream
	 * @param bufferSize
	 *            - buffer size
	 * @return total bytes copied
	 * @throws IOException
	 *             shit happens
	 */
	public static long copy(InputStream from, OutputStream to, int bufferSize) throws IOException {
		if (from == null || to == null)
			return 0;
		byte[] buf = new byte[bufferSize];
		long total = 0;
		while (true) {
			int r = from.read(buf);
			if (r == -1)
				break;
			to.write(buf, 0, r);
			total += r;
		}
		return total;
	}

	/**
	 * From the GoogleIO Commons. Copy an input stream to and output stream.
	 * Uses default buffer size (4K).
	 * 
	 * @param from
	 *            - input stream
	 * @param to
	 *            - output stream
	 * @return total bytes copied
	 * @throws IOException
	 *             shit happens
	 */
	public static long copy(InputStream from, OutputStream to) throws IOException {
		return copy(from, to, BUF_SIZE);
	}
}
