/**
    Copyright (C) 2015  FoxGenesis
 
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.util;

import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @author Seth
 *
 */
public final class ListHelper {
	/**
	 * Calls the action on any element that is present in A but not B
	 * @param a - first list
	 * @param b - second list
	 * @param action - what to do with element
	 */
	public static <T> void forMissing(Collection<T> a, Collection<T> b, Consumer<T> action) {
		getMissing(a,b).forEach(action);
	}
	
	/**
	 * Gets any element that is present in A but not B
	 * @param a - first list
	 * @param b - second list
	 * @param action - what to do with element
	 */
	public static <T> List<T> getMissing(Collection<T> a, Collection<T> b) {
		return a.stream().filter(c -> !b.contains(c)).collect(Collectors.toList());
	}
}
