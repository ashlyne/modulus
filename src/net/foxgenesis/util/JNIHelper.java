/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.util;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Arrays;

import net.foxgenesis.admin.TeamspeakAdmin;
import sun.awt.OSInfo.OSType;

/**
 * @author Seth
 *
 */
public final class JNIHelper {
	private static final OSType os = sun.awt.OSInfo.getOSType();
	private static final File NOFILE = new File("");

	public static void addLibraryFolder(File natives) {
		if(!TeamspeakAdmin.nativesAllowed()) {
			LoggerUtils.warning("JNI Helper","NATIVES ARE DISABLED! not adding!");
			return;
		}
		try {
			File file = Arrays.asList(natives.listFiles()).stream().filter(f -> f.getName()
					.equalsIgnoreCase(os.name())).findFirst().orElse(NOFILE);
			if(file == NOFILE)
				LoggerUtils.error("JNI Helper","No natives folder found for[" + os.name() + "] in " + natives);
			else
				addPath(file);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private static void addPath(File file) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		String l = System.getProperty("java.library.path");
		l = file.getAbsolutePath() + ";" + l;
		System.setProperty("java.library.path", l);
		Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
		fieldSysPath.setAccessible( true );
		fieldSysPath.set( null, null );
		LoggerUtils.info("JNI Helper","Adding [" + file + "] to library path");
	}
}
