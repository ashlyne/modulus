/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.util;

import java.util.Collection;
import java.util.Iterator;

/**
 * Class to help with {@link String}s
 * 
 * @author Seth
 */
public final class StringHelper {

	/**
	 * Counts the amount of the character in the array
	 * 
	 * @param array
	 *            - array to use
	 * @param c
	 *            - {@link Character} to use
	 * @return amount of times the {@link Character} is in the array
	 */
	public static int countChar(char[] array, char c) {
		if (array == null || array.length == 0)
			return 0;
		return countChar(ArrayHelper.rest(array), c) + (array[0] == c ? 1 : 0);
	}

	/**
	 * Counts the amount of the character in the {@link String}
	 * 
	 * @param line
	 *            - {@link String} to use
	 * @param c
	 *            - {@link Character} to use
	 * @return amount of times the {@link Character} is in the {@link String}
	 */
	public static int countChar(String line, char c) {
		return countChar(line.toCharArray(), c);
	}

	/**
	 * Extract text from a given starting and ending point
	 * 
	 * @param line
	 *            - text to use
	 * @param start
	 *            - starting point
	 * @param end
	 *            - ending point
	 * @return extracted text
	 */
	public static String extract(String line, String start, String end) {
		final int s0 = line.indexOf(start) + start.length();
		final int s1 = line.indexOf(end, s0 + 1);
		return line.substring(s0, s1);
	}

	/**
	 * Removes all non-numbers from a {@link String}
	 * 
	 * @param line
	 *            - {@link String} to use
	 * @return {@link String} with only numbers
	 */
	public static String removeNonNumbers(String line) {
		return line.replaceAll("[^\\d.]", "");
	}

	public static String deSplit(Collection<String> array, String delim) {
		return deSplit(array.iterator(), "", delim);
	}

	private static String deSplit(Iterator<String> array, String working, String delim) {
		if (!array.hasNext())
			return working;
		working += array.next() + (array.hasNext() ? delim : "");
		return deSplit(array, working, delim);
	}

	public static String deSplit(String[] array, String delim) {
		return deSplit(array, "", delim);
	}

	private static String deSplit(String[] array, String working, String delim) {
		if (array == null)
			return working;
		working += array[0] + (array.length > 1 ? delim : "");
		return deSplit(ArrayHelper.rest(array), working, delim);
	}
}
