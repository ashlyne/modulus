/**
    Copyright (C) 2015  FoxGenesis
 
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.util;

import static org.fusesource.jansi.Ansi.ansi;

import net.foxgenesis.admin.TeamspeakAdmin;

/**
 * Class to help with console logging
 * 
 * @author Seth
 */
public final class LoggerUtils {

	/**
	 * Render an error to the console
	 * 
	 * @param msg
	 *            - error message
	 */
	public static void error(Object msg) {
		System.err.println(ansi().reset().render("@|red " + msg + "|@"));
	}
	
	public static void error(String title, Object msg) {
		error("[" + title + "]: " + msg.toString());
	}

	/**
	 * Render a warning to the console
	 * 
	 * @param msg
	 *            - warning message
	 */
	public static void warning(Object msg) {
		System.out.println(ansi().reset().render("@|yellow " + msg + "|@"));
	}
	
	public static void warning(String title, Object msg) {
		warning("[" + title + "]: " + msg.toString());
	}

	/**
	 * Render a message to the console.
	 * 
	 * @param msg
	 *            - message to use
	 * @see ansi.render
	 */
	public static void render(String msg) {
		System.out.println(ansi().reset().render(msg));
	}

	public static void debug(Object msg) {
		if (TeamspeakAdmin.isDebug())
			System.out.println("[DEBUG]: " + msg);
	}
	
	public static void debug(String title, Object msg) {
		debug("[" + title + "]: " + msg.toString());
	}

	public static void verbose(Object msg) {
		if (TeamspeakAdmin.isVerbose())
			System.out.println(msg);
	}
	
	public static void verbose(String title, Object msg) {
		verbose("[" + title + "]: " + msg.toString());
	}

	public static void info(Object msg) {
		System.out.println(msg);
	}
	
	public static void info(String title, Object msg) {
		System.out.println("[" + title + "]: " + msg);
	}

	public static void line() {
		System.out.println();
	}

	/**
	 * Print the current stack trace
	 * 
	 * @param msg
	 *            - error message
	 */
	public static void printStack(String msg) {
		new Throwable(msg).printStackTrace();
	}
	
	/**
	 * Dumps a list of all running threads into the debug stream
	 */
	public static void dumpCurrentThreadList() {
		int active = Thread.activeCount();
		LoggerUtils.debug("Currently active threads: " + active);
		Thread all[] = new Thread[active];
		Thread.enumerate(all);

		for (int i = 0; i < active; i++) 
			LoggerUtils.debug(i + ": " + all[i]);
	}
}
