/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package net.foxgenesis.util;

import java.util.Arrays;

/**
 * Helper class for arrays
 * 
 * @author Seth
 */
public final class ArrayHelper {

	/**
	 * Appends an element to an array
	 * 
	 * @param array
	 *            - array to append to
	 * @param element
	 *            - object to append
	 * @return new array
	 */
	public static <T> T[] append(T[] array, T element) {
		T[] d = Arrays.copyOf(array, array.length + 1);
		d[d.length - 1] = element;
		return d;
	}
	
	private static <T> T[] append(T[] array, T[] array2, int index) {
		if(index > array2.length)
			return array;
		array = append(array, array2[index]);
		return append(array,array2,++index);
	}
	
	public static <T> T[] append(T[] array, T[] array2) {
		return append(array,array2,0);
	}
	
	public static byte[] append(byte[] array, byte element) {
		byte[] d = Arrays.copyOf(array, array.length + 1);
		d[d.length - 1] = element;
		return d;
	}
	
	private static byte[] append(byte[] array, byte[] array2, int index) {
		if(index > array2.length)
			return array;
		array = append(array, array2[index]);
		return append(array,array2,++index);
	}
	
	public static byte[] append(byte[] array, byte[] array2) {
		return append(array,array2,0);
	}

	/**
	 * Merge an array of {@link String}s together
	 * 
	 * @param array
	 *            - array to merge
	 * @return merged {@link String}
	 */
	public static String merge(String[] array) {
		return Arrays.asList(array).stream().reduce((a, b) -> a + b).get();
	}

	/**
	 * Drops the first index of the array
	 * 
	 * @param array
	 *            - array to use
	 * @return array without the first index
	 */
	public static char[] rest(char[] array) {
		return array.length > 1 ? Arrays.copyOfRange(array, 1, array.length) : null;
	}

	/**
	 * Drops the first index of the array
	 * 
	 * @param array
	 *            - array to use
	 * @return array without the first index
	 */
	public static <T> T[] rest(T[] array) {
		return array.length > 1 ? Arrays.copyOfRange(array, 1, array.length) : null;
	}
}
