# Modulus #
*alpha*

### About ###

Modulus is a teamspeak 3 administrative bot. It is programmed in Java and has built in hassle free plugin support.

### Plugins ###
Modulus's built in plugin support offers a few things:

1.  Chat Commands
2.  Functions
3.  Events
4.  Native Loader
5.  Complete front end support (No need to deal with backends)

### How do I get set up? ###

1. Java 8
2. Teamspeak 3
3. Download the latest build
4. Run the jar
5. Edit the configuration file
6. Re-run the jar
6. Enjoy

**Continue to the Wiki for tutorials**